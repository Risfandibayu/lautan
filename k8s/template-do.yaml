---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ${CI_ENVIRONMENT_SLUG}-app
  namespace: ${KUBE_NAMESPACE}
  annotations:
    app.gitlab.com/app: ${CI_PROJECT_PATH_SLUG}
    app.gitlab.com/env: ${CI_ENVIRONMENT_SLUG}
spec:
  replicas: ${CI_REPLICAS}
  selector:
    matchLabels:
      app.kubernetes.io/name: ${CI_ENVIRONMENT_SLUG}-app
  strategy:
      type: RollingUpdate
      rollingUpdate:
        maxSurge: 1
        maxUnavailable: 0
  template:
    metadata:
      labels:
        commit: "${CI_COMMIT_SHORT_SHA}"
        app.kubernetes.io/name: ${CI_ENVIRONMENT_SLUG}-app
    spec:
      imagePullSecrets:
        - name: gitlab-registry-${CI_PROJECT_PATH_SLUG}
      containers:
        - name: app
          image: ${CI_APPLICATION_REPOSITORY}
          imagePullPolicy: IfNotPresent
          resources:
            requests:
              memory: "1Gi"
              cpu: "250m"
            limits:
              memory: "4Gi"
              cpu: "1"
          command: ["/bin/sh"]
          args: ["-c", "php artisan migrate --force && php-fpm -D | tail -f storage/logs/laravel.log"]
        - name: worker
          image: ${CI_APPLICATION_REPOSITORY}
          imagePullPolicy: IfNotPresent
          resources:
            requests:
              memory: "200Mi"
              cpu: "100m"
            limits:
              memory: "500Mi"
              cpu: "150m"
          command: ["/bin/sh"]
          args: ["-c", "php artisan queue:work --queue=high,medium,low,default --tries=3 --timeout=120 --daemon --memory=4096 && tail -f storage/logs/laravel.log"]

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ${CI_ENVIRONMENT_SLUG}
  namespace: ${KUBE_NAMESPACE}
  annotations:
    app.gitlab.com/app: ${CI_PROJECT_PATH_SLUG}
    app.gitlab.com/env: ${CI_ENVIRONMENT_SLUG}
spec:
  replicas: ${CI_REPLICAS}
  selector:
    matchLabels:
      app.kubernetes.io/name: ${CI_ENVIRONMENT_SLUG}
  strategy:
      type: RollingUpdate
      rollingUpdate:
        maxSurge: 1
        maxUnavailable: 0
  template:
    metadata:
      labels:
        commit: "${CI_COMMIT_SHORT_SHA}"
        app.kubernetes.io/name: ${CI_ENVIRONMENT_SLUG}
    spec:
      imagePullSecrets:
        - name: gitlab-registry-${CI_PROJECT_PATH_SLUG}
      initContainers:
        - name: assets
          image: ${CI_APPLICATION_REPOSITORY}
          command:
            - sh
            - -c
            - "cp -r /var/www/html/public/* /data"
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - mountPath: /data
              name: pwa-disk
      containers:
        - name: nginx
          image: nginx:latest
          imagePullPolicy: IfNotPresent
          resources:
            requests:
              memory: "200Mi"
              cpu: "100m"
            limits:
              memory: "500Mi"
              cpu: "250m"
          ports:
            - containerPort: 80
              protocol: TCP
              name: http
          volumeMounts:
            - name: nginx-config
              subPath: nginx.conf
              mountPath: /etc/nginx/nginx.conf
              readOnly: true
            - name: nginx-certs-keys
              mountPath: "/etc/nginx/ssl/"
              readOnly: true
            - name: pwa-disk
              mountPath: /var/www/html/public
              readOnly: true
          livenessProbe:
            failureThreshold: 3
            httpGet:
              path: /healthz
              port: 8080
              scheme: HTTP
            initialDelaySeconds: 10
            periodSeconds: 2
            successThreshold: 1
            timeoutSeconds: 2
          readinessProbe:
            failureThreshold: 3
            httpGet:
              path: /healthz
              port: 8080
              scheme: HTTP
            initialDelaySeconds: 10
            periodSeconds: 2
            successThreshold: 2
            timeoutSeconds: 2
      volumes:
        - name: nginx-config
          configMap:
            name: ${CI_ENVIRONMENT_SLUG}-nginx
        - name: nginx-certs-keys
          secret:
            secretName: nginx-certs-keys
        - name: pwa-disk
          emptyDir: {}

---
apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
  name: ${CI_ENVIRONMENT_SLUG}
  namespace: ${KUBE_NAMESPACE}
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: ${CI_ENVIRONMENT_SLUG}
  minReplicas:  ${CI_REPLICAS}
  maxReplicas: 3
  metrics:
  - resource:
      name: memory
      target:
        averageUtilization: 90
        type: Utilization
    type: Resource
  - resource:
      name: cpu
      target:
        averageUtilization: 95
        type: Utilization
    type: Resource

---
apiVersion: autoscaling/v2beta2
kind: HorizontalPodAutoscaler
metadata:
  name: ${CI_ENVIRONMENT_SLUG}-app
  namespace: ${KUBE_NAMESPACE}
spec:
  scaleTargetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: ${CI_ENVIRONMENT_SLUG}-app
  minReplicas:  ${CI_REPLICAS}
  maxReplicas: 3
  metrics:
  - resource:
      name: memory
      target:
        averageUtilization: 70
        type: Utilization
    type: Resource
  - resource:
      name: cpu
      target:
        averageUtilization: 70
        type: Utilization
    type: Resource

---
kind: ConfigMap
apiVersion: v1
metadata:
  name: ${CI_ENVIRONMENT_SLUG}-nginx
  namespace: ${KUBE_NAMESPACE}
data:
  nginx.conf: |
    user www-data;
    worker_processes auto;
    pid /run/nginx.pid;

    events {}
    http {
    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;
    keepalive_timeout 65;
    types_hash_max_size 2048;

    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    gzip on;
    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_buffers 16 8k;
    gzip_min_length 256;
    gzip_http_version 1.1;
    gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;

    server_tokens off;

    fastcgi_cache_path /dev/shm levels=1:2 keys_zone=laravel:100m;
    fastcgi_cache_key "$scheme$request_method$host$request_uri$query_string";
    proxy_set_header X-Real-IP       $proxy_protocol_addr;
    proxy_set_header X-Forwarded-For $proxy_protocol_addr;

        server {
            listen 8080 default_server;
            location /healthz {
            access_log off;
            return 200 "healthy\n";
            }
        }

        server {
            listen 80  proxy_protocol;
            server_name ${CI_TLS_HOST};

            set_real_ip_from 0.0.0.0/0;
            real_ip_header proxy_protocol;
            real_ip_recursive on;

            root   /var/www/html/public;
            index  index.php index.html;

            # location ^~ /.well-known/acme-challenge { root /var/www/certbot; }
            location /nginx_status {
                stub_status on;
                access_log   off;
                allow all;
            }

            # Redirect all http to https
            location / { return 301 https://$server_name$request_uri; }

            # URL exception so anything that goes by the URL /channel/mandiri will just work as it is
            location /channel/mandiri {
              try_files $uri $uri/ /index.php?$query_string;
            }

            location /notify/muamalat {
              try_files $uri $uri/ /index.php?$query_string;
            }

            location ~ \.php$ {
                try_files     $uri =404;
                fastcgi_cache laravel;
                fastcgi_cache_valid 200 301 302 60m;
                fastcgi_ignore_headers Cache-Control Expires Set-Cookie;
                fastcgi_no_cache $http_authorization $cookie_laravel_session;
                fastcgi_cache_lock on;
                fastcgi_cache_lock_timeout 10s;

                add_header X-Proxy-Cache $upstream_cache_status;

                fastcgi_pass   ${CI_ENVIRONMENT_SLUG}-app:9000;
                fastcgi_index  index.php;
                fastcgi_param  SCRIPT_FILENAME $document_root$fastcgi_script_name;
                fastcgi_read_timeout 900s;
                include        fastcgi_params;

                fastcgi_cache_use_stale error timeout updating invalid_header http_500 http_503;
                fastcgi_cache_min_uses 1;
                fastcgi_cache_bypass $skip_cache $http_pragma;
                fastcgi_no_cache $skip_cache $http_pragma;
            }

            location ~ /\.ht {
                deny all;
            }
        }


        server {
            listen 443  ssl proxy_protocol;
            server_name ${CI_TLS_HOST};

            set_real_ip_from 0.0.0.0/0;
            real_ip_header proxy_protocol;
            real_ip_recursive on;

            server_tokens off;

            ssl_certificate /etc/nginx/ssl/lautan.io.crt;
            ssl_certificate_key /etc/nginx/ssl/lautan.io.key;
            ssl_dhparam /etc/nginx/ssl/ssl-dhparams.pem;

            access_log                /dev/stdout;
            error_log                 /dev/stderr info;

            ssl_session_cache shared:le_nginx_SSL:10m;
            ssl_session_timeout 1440m;
            ssl_session_tickets off;

            ssl_protocols TLSv1.2 TLSv1.3;
            ssl_prefer_server_ciphers off;

            ssl_ciphers "ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384";

            root   /var/www/html/public;
            index  index.php index.html;

            client_max_body_size 100M;
            # add for test pentest by edi
            # client_body_buffer_size 200K;
            # client_header_buffer_size 2k;
            # large_client_header_buffers 3 1k;
            #end for

            add_header X-Frame-Options "SAMEORIGIN";
            add_header X-XSS-Protection "1; mode=block";
            add_header X-Content-Type-Options "nosniff";

            # Active (10053 - Apache Range Header DoS (CVE-2011-3192))
            if ($http_range ~ "(?:\d*\s*-\s*\d*\s*,\s*){5,}") {
            return 416;
            }

            # Block Ddos byte-range request
            proxy_set_header Request-Range "";

            # Closing Slow Connections
            client_body_timeout         5s;
            client_header_timeout       5s;

            # Compression

            # Enable Gzip compressed.
            gzip on;

            # Enable compression both for HTTP/1.0 and HTTP/1.1.
            gzip_http_version  1.1;

            # Compression level (1-9).
            # 5 is a perfect compromise between size and cpu usage, offering about
            # 75% reduction for most ascii files (almost identical to level 9).
            gzip_comp_level    5;

            # Don't compress anything that's already small and unlikely to shrink much
            # if at all (the default is 20 bytes, which is bad as that usually leads to
            # larger files after gzipping).
            gzip_min_length    256;

            # Compress data even for clients that are connecting to us via proxies,
            # identified by the "Via" header (required for CloudFront).
            gzip_proxied       any;

            # Tell proxies to cache both the gzipped and regular version of a resource
            # whenever the client's Accept-Encoding capabilities header varies;
            # Avoids the issue where a non-gzip capable client (which is extremely rare
            # today) would display gibberish if their proxy gave them the gzipped version.
            gzip_vary          on;

            # Compress all output labeled with one of the following MIME-types.
            gzip_types
            application/atom+xml
            application/javascript
            application/json
            application/rss+xml
            application/vnd.ms-fontobject
            application/x-font-ttf
            application/x-web-app-manifest+json
            application/xhtml+xml
            application/xml
            font/opentype
            image/svg+xml
            image/x-icon
            text/css
            text/plain
            text/x-component;
            # text/html is always compressed by HttpGzipModule

            location / {
                try_files $uri $uri/ /index.php?$query_string;
            }

            # Redirect https request of this exception URL to http required for h2h bank mandiri
            location /channel/mandiri {
                return 301 http://$server_name$request_uri;
            }

            location /notify/muamalat {
                return 301 http://$server_name$request_uri;
            }

            set $skip_cache 0;

            # POST requests and urls with a query string should always go to PHP
            if ($request_method = POST) {
                set $skip_cache 1;
            }
            if ($query_string != "") {
                set $skip_cache 1;
            }

            # Don't cache uris containing the following segments
            if ($request_uri ~* "/*") {
                set $skip_cache 1;
            }

            location ~ \.php$ {
                try_files     $uri =404;
                fastcgi_cache laravel;
                fastcgi_cache_valid 200 301 302 60m;
                fastcgi_ignore_headers Cache-Control Expires Set-Cookie;
                fastcgi_no_cache $http_authorization $cookie_laravel_session;
                fastcgi_cache_lock on;
                fastcgi_cache_lock_timeout 10s;

                add_header X-Proxy-Cache $upstream_cache_status;

                fastcgi_pass   ${CI_ENVIRONMENT_SLUG}-app:9000;
                fastcgi_index  index.php;
                fastcgi_param  SCRIPT_FILENAME $document_root$fastcgi_script_name;
                fastcgi_read_timeout 900s;
                include        fastcgi_params;

                fastcgi_cache_use_stale error timeout updating invalid_header http_500 http_503;
                fastcgi_cache_min_uses 1;
                fastcgi_cache_bypass $skip_cache $http_pragma;
                fastcgi_no_cache $skip_cache $http_pragma;

            }

            location ~* \.(jpg|jpeg|png|gif|ico|css|js|eot|ttf|woff|woff2)$ {
                expires max;
                add_header Cache-Control public;
                access_log off;
            }

            location ~ /\.ht {
                deny all;
            }
        }
    }
---
apiVersion: v1
kind: Service
metadata:
  name: ${CI_ENVIRONMENT_SLUG}
  namespace: ${KUBE_NAMESPACE}
  labels:
    app.kubernetes.io/name: ${CI_ENVIRONMENT_SLUG}
  annotations:
    service.beta.kubernetes.io/do-loadbalancer-name: ${KUBE_NAMESPACE}
    service.beta.kubernetes.io/do-loadbalancer-hostname: ${CI_TLS_HOST}
    service.beta.kubernetes.io/do-loadbalancer-enable-backend-keepalive: "true"
    service.beta.kubernetes.io/do-loadbalancer-enable-proxy-protocol: "true"
    service.beta.kubernetes.io/do-loadbalancer-protocol: "http"
    service.beta.kubernetes.io/do-loadbalancer-http2-ports: "443"
    service.beta.kubernetes.io/do-loadbalancer-tls-passthrough: "true"
spec:
  type: LoadBalancer
  ports:
  - name: http
    port: 80
    protocol: TCP
    targetPort: 80
  - name: https
    port: 443
    protocol: TCP
    targetPort: 443
  - name: healthz
    port: 8080
    protocol: TCP
    targetPort: 8080
  selector:
    app.kubernetes.io/name: ${CI_ENVIRONMENT_SLUG}
---
apiVersion: v1
kind: Service
metadata:
  name: ${CI_ENVIRONMENT_SLUG}-app
  namespace: ${KUBE_NAMESPACE}
  labels:
    app.kubernetes.io/name: ${CI_ENVIRONMENT_SLUG}-app
spec:
  type: ClusterIP
  ports:
  - name: http
    port: 9000
    protocol: TCP
    targetPort: 9000
  selector:
    app.kubernetes.io/name: ${CI_ENVIRONMENT_SLUG}-app
---
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: ${CI_ENVIRONMENT_SLUG}-scheduler
  namespace: ${KUBE_NAMESPACE}
spec:
  concurrencyPolicy: Replace
  failedJobsHistoryLimit: 1
  startingDeadlineSeconds: 30
  successfulJobsHistoryLimit: 1
  schedule: "*/1 * * * *"
  jobTemplate:
    spec:
      activeDeadlineSeconds: 120
      backoffLimit: 1
      template:
        spec:
          containers:
          - name: scheduler
            image: ${CI_APPLICATION_REPOSITORY}
            command: ["/bin/sh"]
            args: ["-c", "php artisan schedule:run -vv --no-interaction >> storage/logs/crontab.log | tail -f storage/logs/crontab.log"]
            imagePullPolicy: IfNotPresent
          restartPolicy: OnFailure
          imagePullSecrets:
            - name: gitlab-registry-${CI_PROJECT_PATH_SLUG}
---
apiVersion: policy/v1beta1
kind: PodDisruptionBudget
metadata:
  name: ${CI_ENVIRONMENT_SLUG}-app
  namespace: ${KUBE_NAMESPACE}
spec:
  maxUnavailable: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: ${CI_ENVIRONMENT_SLUG}-app
---
apiVersion: policy/v1beta1
kind: PodDisruptionBudget
metadata:
  name: ${CI_ENVIRONMENT_SLUG}
  namespace: ${KUBE_NAMESPACE}
spec:
  maxUnavailable: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: ${CI_ENVIRONMENT_SLUG}
