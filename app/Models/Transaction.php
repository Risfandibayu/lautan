<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    public function currency()
    {
        return $this->belongsTo('App\Models\Currency');
    }

    public function getStatusLabel()
    {
        switch ($this->status) {
            case 1:
                return '<span class="badge badge-pill badge-success">Success</span>';
                break;
            case 0:
                return '<span class="badge badge-pill badge-dark">Pending</span>';
                break;
        }
    }

    public function getStatus()
    {
        switch ($this->status) {
            case 1:
                return 'Success';
                break;
            case 0:
                return 'Pending';
                break;
        }
    }

    public function getType()
    {
        switch ($this->type) {
            case 'deposit':
                return 'Deposit';
                break;
            case 'withdraw':
                return 'Withdraw';
                break;
        }
    }
}
