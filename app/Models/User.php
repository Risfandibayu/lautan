<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function agent()
    {
        return $this->belongsTo('App\Models\AgentRole','agent_role_id');
    }

    public function getAvatar()
    {
        if($this->avatar) {
            return url('/public/uploads/avatar/'.$this->avatar);
        } else {
            return url('/public/assets/images/profile.png');
        }
    }

    public function getStatusLabel()
    {
        switch ($this->status) {
            case '1':
                return '<span class="badge bg-warning">Unverified</span>';
            break;
            case '2':
                return '<span class="badge bg-info">Under Verification</span>';
            break;
            case '3':
                return '<span class="badge bg-success">Verified</span>';
            break;
        }
    }
}
