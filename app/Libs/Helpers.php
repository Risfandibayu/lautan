<?php

    function fixDateInput($value) {
        $exp    = explode('-', $value);
        $result = $exp[2].'-'.$exp[1].'-'.$exp[0];
        return $result;
    }

    function fixDateInputPicker($value) {
        //$exp    = explode('-', $value);
        $result = substr($value,0,10);
        return $result;
    }

    function rupiah($value = '') {
        if($value != '' || $value != '0'){
            $val = number_format($value,0,',','.');
        }else{
            $val = '0';
        }
        return $val;
        
    }

    function unmask($value)
    {
        if($value != '' && $value > 0) {
            return str_replace('.', '', $value);
        } else {
            return 0;
        }
    }

    function persen($value) {
        if($value > 0) {
            return str_replace('.', ',', $value);
        } else {
            return 0;
        }
    }

    function dateTimeIndo($tanggal)
    {
        if(empty($tanggal) || !isset($tanggal)){
            $result = '-';
        }else{
            $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
            $tahun = date("Y", strtotime($tanggal));
            $bulan = date("m", strtotime($tanggal));
            $tgl = date("d", strtotime($tanggal));
            // $tahun = substr($tanggal, 0, 4);
            // $bulan = substr($tanggal, 5, 2);
            // $tgl   = substr($tanggal, 8, 2);
            $waktu   = date("H:i:s", strtotime($tanggal));
            $result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun. " ".$waktu;     
        }
        return($result);
    }

    function dateIndo($tanggal)
    {
        if(empty($tanggal) || !isset($tanggal)){
            $result = '-';
        }else{
            $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
            $tahun  = date("Y", strtotime($tanggal));
            $bulan  = date("m", strtotime($tanggal));
            $tgl    = date("d", strtotime($tanggal));
            $result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;     
        }
        return($result);
    }

    function dateSimple($tanggal)
    {
        if(empty($tanggal) || !isset($tanggal)){
            $result = '-';
        }else{
            $tahun  = date("Y", strtotime($tanggal));
            $bulan  = date("m", strtotime($tanggal));
            $tgl    = date("d", strtotime($tanggal));
            $result = $tgl . "-" . $bulan . "-". $tahun;        
        }
        return($result);
    }

    function terbilang($x) {
        $x = abs($x);
        $angka = array("", "satu", "dua", "tiga", "empat", "lima",
        "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($x <12) {
            $temp = " ". $angka[$x];
        } else if ($x <20) {
            $temp = terbilang($x - 10). " belas";
        } else if ($x <100) {
            $temp = terbilang($x/10)." puluh". terbilang($x % 10);
        } else if ($x <200) {
            $temp = " seratus" . terbilang($x - 100);
        } else if ($x <1000) {
            $temp = terbilang($x/100) . " ratus" . terbilang($x % 100);
        } else if ($x <2000) {
            $temp = " seribu" . terbilang($x - 1000);
        } else if ($x <1000000) {
            $temp = terbilang($x/1000) . " ribu" . terbilang($x % 1000);
        } else if ($x <1000000000) {
            $temp = terbilang($x/1000000) . " juta" . terbilang($x % 1000000);
        } else if ($x <1000000000000) {
            $temp = terbilang($x/1000000000) . " milyar" . terbilang(fmod($x,1000000000));
        } else if ($x <1000000000000000) {
            $temp = terbilang($x/1000000000000) . " trilyun" . terbilang(fmod($x,1000000000000));
        }     
            return $temp;
    }


    function rounding($amount)
    {
        $amount = ceil($amount);
        if (substr($amount, -3) > 499){
            $total = round($amount, -3);
        } else {
            $total = round($amount, -3) + 500;
        } 
        return $total;
    }

    function ageCount($tanggal_lahir)
    {
        list($year,$month,$day) = explode("-",$tanggal_lahir);
        $year_diff  = date("Y") - $year;
        $month_diff = date("m") - $month;
        $day_diff   = date("d") - $day;
        if ($month_diff < 0) $year_diff--;
            elseif (($month_diff==0) && ($day_diff < 0)) $year_diff--;
        return $year_diff;
    }

    function convertDate($date)
    {
        $arr = explode('-', $date);
        $newDate = $arr[2].'-'.$arr[1].'-'.$arr[0];
        return $newDate;
    }

    function dayIndo($day) {
        switch($day) {
            case 'Sun':
                $ret = "Minggu";
            break;
    
            case 'Mon':			
                $ret = "Senin";
            break;
    
            case 'Tue':
                $ret = "Selasa";
            break;
    
            case 'Wed':
                $ret = "Rabu";
            break;
    
            case 'Thu':
                $ret = "Kamis";
            break;
    
            case 'Fri':
                $ret = "Jumat";
            break;
    
            case 'Sat':
                $ret = "Sabtu";
            break;
            
            default:
                $ret = "-";		
            break;
        }
    
        return $ret;
    
    }

    function day2int($day) {
        switch($day) {
            case 'Sun':
                return 7;
            break;
            case 'Mon':			
                return 1;
            break;
            case 'Tue':
                return 2;
            break;
            case 'Wed':
                return 3;
            break;
            case 'Thu':
                return 4;
            break;
            case 'Fri':
                return 5;
            break;
            case 'Sat':
                return 6;
            break;
            default:
                return 0;
            break;
        }
    }

    function int2day($day) {
        switch($day) {
            case 1:			
                return "Senin";
            break;
            case 2:
                return "Selasa";
            break;
            case 3:
                return "Rabu";
            break;
            case 4:
                return "Kamis";
            break;
            case 5:
                return "Jumat";
            break;
            case 6:
                return "Sabtu";
            break;
            case 7:
                return "Minggu";
            break;
            default:
                return "-";		
            break;
        }
    }

    function monthIndo($month) {
        switch($month) {
            case '1':
                return "Januari";
            break;
    
            case '2':			
                return "Feburari";
            break;
    
            case '3':
                return "Maret";
            break;
    
            case '4':
                return "April";
            break;
    
            case '5':
                return "Mei";
            break;
    
            case '6':
                return "Juni";
            break;
    
            case '7':
                return "Juli";
            break;
            
            case '8':
                return "Agustus";
            break;
            
            case '9':
                return "September";
            break;

            case '10':
                return "Oktober";
            break;

            case '11':
                return "November";
            break;

            case '12':
                return "Desember";
            break;
            
            default:
                return "-";		
            break;
        }
    }

    function monthRomawi($bln){
        switch ($bln){
            case 1: 
                return "I";
                break;
            case 2:
                return "II";
                break;
            case 3:
                return "III";
                break;
            case 4:
                return "IV";
                break;
            case 5:
                return "V";
                break;
            case 6:
                return "VI";
                break;
            case 7:
                return "VII";
                break;
            case 8:
                return "VIII";
                break;
            case 9:
                return "IX";
                break;
            case 10:
                return "X";
                break;
            case 11:
                return "XI";
                break;
            case 12:
                return "XII";
                break;
        }
}

    function phoneFormat($nohp)
	{

	    $nohp = str_replace(" ", "", $nohp);
	    $nohp = str_replace("(", "", $nohp);
	    $nohp = str_replace(")", "", $nohp);
	    $nohp = str_replace(".", "", $nohp);
		
	    if (!preg_match('/[^+0-9]/', trim($nohp))) {

	        if (substr(trim($nohp), 0, 3) == '+62') {
	            $hp = '0' . substr(trim($nohp), 3);
	        } elseif (substr(trim($nohp), 0, 1) == '0') {
				$hp = trim($nohp);
	        } elseif (substr(trim($nohp), 0, 2) == '62') {
                $hp = '0' . substr(trim($nohp), 2);
            } elseif (substr(trim($nohp), 0, 1) != '0') {
				$hp = trim('0'.$nohp);
	        } else {
				$hp = $nohp;	
			}

	    } else {
	        $hp = $nohp;
	    }

	    return $hp;
    }
    
    function format_date($tanggal)
	{
        if($tanggal) {
            $bulan = array (1 =>   'January',
                        'February',
                        'March',
                        'April',
                        'May',
                        'June',
                        'July',
                        'August',
                        'September',
                        'October',
                        'November',
                        'December'
                    );
            $split = explode('-', $tanggal);
            return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
        } else {
            return null;
        }
    }
    
    function formatParkingDate($tanggal)
	{
		$date = explode(' ', $tanggal);
		$split = explode('-', $date[0]);

		switch ($split[1]) {
			case 'Jan':
				$month = '01';
				break;
			case 'Feb':
				$month = '02';
				break;
			case 'Mar':
				$month = '03';
				break;
			case 'Apr':
				$month = '04';
				break;
			case 'May':
				$month = '05';
				break;
			case 'Jun':
				$month = '06';
				break;
			case 'Jul':
				$month = '07';
				break;
			case 'Aug':
				$month = '08';
				break;
			case 'Sep':
				$month = '09';
				break;
			case 'Oct':
				$month = '10';
				break;
			case 'Nov':
				$month = '11';
				break;
			case 'Dec':
				$month = '12';
				break;
		}
		return $split[2].'-'.$month.'-'.$split[0].' '.$date[1];
    }
    
    function get_day($tanggal)
	{
		$newdate            = strtotime($tanggal);
        $textdate           = date("l", $newdate);
		return $textdate;
    }
    
    function base64ToImage($base64String,$fileName,$destination) {
        if($base64String) {
            // set location
            $destinationPath                    = public_path().'/uploads/'.$destination;
            if(!File::exists($destinationPath)) {
                File::makeDirectory($destinationPath, $mode = 0777, true, true);
            }
            // upload image
            $image                  = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '',$base64String));
            File::put($destinationPath .'/'. $fileName, $image);
            return $fileName;
        } else {
            return '';
        }
    }

?>
