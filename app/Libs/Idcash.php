<?php
namespace App\Libs;

use Illuminate\Support\Facades\Config;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client;
use function GuzzleHttp\json_decode;
use function GuzzleHttp\json_encode;
use cURL;
use DB;
use App\Models\Datalog;
Class Idcash {
    
    public function callApi($data)
    {

        $body         = $data['body'];
        $requestBody  = strtolower(hash('sha256', json_encode($data['body'], JSON_UNESCAPED_SLASHES)));
        $secret       = $data['secret'];
        $va           = $data['va'];
        $stringToSign = $data['method'] . ':' . $va . ':' . $requestBody . ':' . $secret;
        $signature    = hash_hmac('sha256', $stringToSign, $secret);
        $timestamp    = Date('YmdHis');
        $url          = $data['url'];

        $request = cURL::newRequest($data['method'], $url, $data['body'])
            //->setHeader('Content-Type', 'application/json')
            ->setHeader('va', $va)
            ->setHeader('signature', $signature)
            ->setHeader('timestamp', $timestamp)
            ->setHeader('CURLOPT_POST', true);
    
        try {
            $response           = $request->send();
            $data               = json_decode($response);
            $status             = 200;
        } catch (RequestException $e) {
            $data['Status']     = 400;
            $data['Data']       = $e->getResponse();
            $data['Message']    = $e->getMessage();
            $status             = 400;
        }
        
        $headers = [
            'Content-Type'              => 'application/json',
            'va'                        => $va,
            'signature'                 => $signature,
            'timestamp'                 => $timestamp
        ];

        // $client = new Client([
        //     'headers' => $headers
        // ]);
        // try {
        //     $response = $client->request(strtoupper($data['method']), $url, [
        //         'body' => $body,
        //     ]);
        //     // $data =  $response->getBody()->getContents();
        //     $data   = json_decode($response->getBody()->getContents());
        //     $status = 200;
        //     // $data['data']   = $response->getBody();
        // } catch (RequestException $e) {
        //     // $data['status'] = 400;
        //     $data['Status']    = 400;
        //     $data['Data']      = $e->getResponse();
        //     $data['Message']   = $e->getMessage();
        //     $status            = 400;
        //     // $data['msg']    = 'invalid request';
        // }
       
        // LOG RESPONSE
        // $filename    = storage_path() . '/logs/idcash/' . date('Y-m-d') . '.log';
        // $directory   = dirname($filename);
        // if (!is_dir($directory)) {
        //     mkdir($directory, 0777, true);
        // }
         
        $clog       = "TIME: ".date('Y-m-d H:i:s')." \n";
        $clog      .= 'IP: ' . $_SERVER['REMOTE_ADDR'] . " \n";
        $clog      .= "StringToSign: " . $stringToSign . " \n";
        $clog      .= "Request (Header): " . json_encode($headers, JSON_UNESCAPED_SLASHES) . " \n";
        $clog      .= "Request (Body): " . json_encode($body, JSON_UNESCAPED_SLASHES) . " \n";
        $clog      .= "Response Status: " . $status . " \n";
        $clog      .= "Response: " . json_encode($data, JSON_UNESCAPED_SLASHES) . " \n";
        $clog      .= "-----------------------------------------\n\n";
        // $h          = file_put_contents($filename, $clog, FILE_APPEND);

        DB::beginTransaction();
        try {
            $datalog            = NEW Datalog;
            $datalog->channel   = 'idcash';
            $datalog->log       = $clog;
            $datalog->save();
            DB::commit();
        } catch (ClientException $e) {
            DB::rollback();
        }
        
        if($status == 200) {
            return $data;
        }
        // return $data;
        return false;
        // return response()->json($data, $data['status']);  
    }

    public static function encryptQR($data, $key){
        $password = substr(openssl_digest($key, 'sha1', true), 0, 16);
        return trim(openssl_encrypt($data, 'AES-128-ECB', $password, 0));
    }

    public static function decryptQR($data, $key){
        $password = substr(openssl_digest($key, 'sha1', true), 0, 16);
        return trim(openssl_decrypt($data, 'AES-128-ECB', $password, 0));
    }
    
}