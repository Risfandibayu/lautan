<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libs\Idcash;
use Auth;
class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu               = 'account';
        return view('account.index')->with(compact('menu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function idcash(Request $request)
    {
        $user                       = Auth::user();

        $va                         = env('IPAYMU_VA');
        $api                        = env('IPAYMU_KEY');
        $integration_key            = Idcash::encryptQR($va . '|' . $api, env('IDCASH_KEY'));
        // register to idcash ambil va dan key
        $idcash                     = NEW Idcash;
        $send['url']                = env('IDCASH_URL').'register';
        $send['method']             = 'POST';
        $body['name']               = $user->name;
        $body['country_code']       = '+62';
        $body['phone_number']       = substr(trim($user->phone), 1);
        $body['email']              = $user->email;
        $body['integration_key']    = $integration_key;
        $body['request']            = 'register';
        $send['va']                 = env('IDCASH_VA');
        $send['secret']             = env('IDCASH_KEY');
        $send['body']               = $body;
        $post                       = $idcash->callApi($send);
        if($post AND $post->status == 201) {
            $user->idcashVa         = $post->member->va;
            $user->idcashKey        = $post->member->api_key;
            $user->idcashStatus     = 1;
            $user->save();
            return redirect('/idcash/done')->with('success','Activate IDCash Success');
        } else {
            return redirect('/withdraw')->with(['error' => 'Activate IDCash Error']);
        }
        // $user->idcashStatus = 1;
        // $user->idcashVa     = '118908970832837';
        // $user->save();
        // return redirect('/withdraw')->with('success','Activate IDCash Success');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function idcashConnect(Request $request)
    {
        $user                       = Auth::user();

        $va                         = env('IPAYMU_VA');
        $api                        = env('IPAYMU_KEY');
        $integration_key            = Idcash::encryptQR($va . '|' . $api, env('IDCASH_KEY'));
        // register to idcash ambil va dan key
        $idcash                     = NEW Idcash;
        $send['url']                = env('IDCASH_URL').'add-old-card';
        $send['method']             = 'POST';
        $body['va_num']             = $request->input('idcashVa');
        $body['card_num']           = $request->input('idcashCard');
        $body['pin']                = $request->input('idcashPin');
        $body['request']            = 'add_card';
        $send['va']                 = env('IDCASH_VA');
        $send['secret']             = env('IDCASH_KEY');
        $send['body']               = $body;
        $post                       = $idcash->callApi($send);
        if($post AND $post->status == 201) {
            $user->idcashVa         = $post->member->va;
            $user->idcashKey        = $post->member->api_key;
            $user->idcashStatus     = 1;
            $user->save();
            return redirect('/idcash/done')->with('success','Activate IDCash Success');
        } else {
            return redirect('/withdraw')->with(['error' => 'Activate IDCash Error']);
        }
        // $user->idcashStatus = 1;
        // $user->idcashVa     = '118908970832837';
        // $user->save();
        // return redirect('/withdraw')->with('success','Activate IDCash Success');
    }

    public function idcashDone()
    {
        $menu = 'balance';
        return view('account.idcash')->with(compact('menu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
