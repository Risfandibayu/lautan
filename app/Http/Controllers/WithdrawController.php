<?php

namespace App\Http\Controllers;

include __DIR__.'/SendMe.php';
use Illuminate\Http\Request;
use Auth;
use Hash;
use DB;
use Log;
use Mail;
use App\Libs\Binance;
use App\Libs\Ipaymu;
use App\Libs\Idcash;
use App\Models\Currency;
use App\Models\Bank;
use App\Models\Balance;
use App\Models\Transaction;
use App\Models\UserPin;
use DateTime;
use Illuminate\Support\Facades\Date;
use SendMe;

class WithdrawController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // if(!Auth::user()->tokocryptoKey OR !Auth::user()->tokocryptoSecret) {
        //     return redirect('/tokocrypto')->with('warning','Silahkan hubungkan akun Tokocrypto Anda terlebih dahulu sebelum dapat mengakses menu Penarikan');
        if(Auth::user()->status != 3) {
            return redirect('/verification')->with('warning','Silahkan lakukan verifikasi terlebih dahulu sebelum dapat mengakses menu Penarikan');
        } else {
            $menu = 'balance';
            $row                = Currency::where('active',1)->orderBy('name')->get();
            $bank               = Bank::where('active',1)->orderBy('name')->get();
            return view('withdraw.index')->with(compact('menu','row','bank'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getInfo()
    {
        $menu = 'balance';
        return view('withdraw.info')->with(compact('menu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postInfo(Request $request)
    {
        $user = Auth::user();
        $balance = Balance::where('user_id',$user->id)->where('currency_id',$request->input('code'))->orderBy('created_at', 'DESC')->first();
        if($balance) {
            $blc = $balance->amount;
        } else {
            $blc = 0;
        }
        if (!$request->input('amount')) {
            return redirect('/withdraw')->withInput()->with('error','Please input withdraw amount');
        } elseif ($request->input('amount') > $blc) {
            return redirect('/withdraw')->withInput()->with('error','Not enough balance');
        } else {
            $currency                   = Currency::where('active',1)->where('id',$request->input('code'))->first();
            
            $bank_acc                   = db::table('user_bank')->where('id', $request->bank_acc)->first();
            $bank                       = Bank::where('code',$bank_acc->bank_code)->first();

            $binance                                = NEW Binance;
            $send['url']                            = '/ticker/24hr';
            $body['symbol']                         = $currency->code;
            $send['body']                           = $body; 
            $post                                   = $binance->callApi($send);

            $amount                                 = $post->lastPrice * $request->input('amount');

            $newpin                                 = random_int(100000, 999999);
            $pin                                    = NEW UserPin;
            $pin->user_id                           = $user->id;
            $pin->pin                               = Hash::make($newpin);
            $pin->save();

            $data = [
                'name'  => $user->name, 
                'pin'   => $newpin
            ];

            Mail::send('emails.withdrawpin', $data, function($message)use($data, $user) {
                $message->to($user->email)
                        ->subject('Withdraw Confirmation');
            });

            $request->session()->put('type', $request->input('type'));
            $request->session()->put('amount', $request->input('amount'));
            $request->session()->put('amountIdr', $amount);
            $request->session()->put('code', $currency->id);
            $request->session()->put('coin', $currency->code);
            $request->session()->put('bankCode', $bank_acc->bank_code);
            $request->session()->put('bankName', $bank->name);
            $request->session()->put('bankAccount', $bank_acc->bank_account);
            $request->session()->put('bankNumber', $bank_acc->account_number);
            $request->session()->put('bankID', $bank_acc->id);
            // $request->session()->put('pin', $newpin);
            $user = Auth::user();
            DB::beginTransaction();
            try {
                
            if($bank_acc->isReg == 0){
                
                $sendme = new SendMe();	
                $sendme->enableProd();
                $reg = $sendme->register([	
                    "beneficiary_account" 		=> $bank_acc->account_number,
                    "beneficiary_account_name" 	=> $bank_acc->bank_account,
                    "beneficiary_va_name" 		=> $bank_acc->bank_account,
                    "beneficiary_bank_code" 	=> $bank_acc->bank_code,
                    "beneficiary_bank_branch" 	=> $bank->name,
                    "beneficiary_region_code" 	=> "0102",
                    "beneficiary_country_code" 	=> "ID",
                    "beneficiary_purpose_code" 	=> "1",
                ]);
                
                $reg2 = $sendme->confirm([
                    // "virtual_account"            => "9920001189",
                    "virtual_account"            => "9847090569",
                    "beneficiary_account" 		=> $bank_acc->account_number,
                    "beneficiary_account_name" 	=> $bank_acc->bank_account,
                    "beneficiary_va_name" 		=> $bank_acc->bank_account,
                    "beneficiary_bank_code" 	=> $bank_acc->bank_code,
                    "beneficiary_bank_branch" 	=> $bank->name,
                    "beneficiary_region_code" 	=> "0102",
                    "beneficiary_country_code" 	=> "ID",
                    "beneficiary_purpose_code" 	=> "1",
                    "bank_account_number"=> $bank_acc->account_number,
                    "bank_account_name"=> $bank_acc->bank_account,
                    "confirm" => '2',
                ]);
                $ba = db::table('user_bank')->where('id',$bank_acc->id)->update([
                            'bva' => $reg2['beneficiary_virtual_account'],
                            'ban' => $reg2['beneficiary_account_name'],
                            'isReg' => 1
                        ]);
                
                if($user->trx_fp == null || empty($user->trx_fp) || $user->trx_fp == 0){
                    $user->trx_fp =   ($user->id * 100000) + 1;
                }else{
                    $user->trx_fp  = $user->trx_fp + 1;
                }
                ;
                $user->save();
                DB::commit();
            }else{
                if($user->trx_fp == null || empty($user->trx_fp) || $user->trx_fp == 0){
                    $user->trx_fp =   ($user->id * 100000) + 1;
                }else{
                    $user->trx_fp  = $user->trx_fp + 1;
                }
                ;
                $user->save();
                DB::commit();
            }
            

            } catch (ClientException $e) {
                DB::rollback();
                Log::info($e->getMessage());
                return redirect('/withdraw')->withInput()->with('error','Withdraw error, please try again later');
            }
            
            
            
            return redirect('/withdraw/info');
            
            // return json_encode($reg2);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   
    
    public function store(Request $request)
    {
        $user = Auth::user();
        $balance = Balance::where('user_id',$user->id)->where('currency_id',$request->input('code'))->orderBy('created_at', 'DESC')->first();
        if($balance) {
            $blc = $balance->amount;
        } else {
            $blc = 0;
        }
        $pin = UserPin::where('user_id',$user->id)->orderBy('id','DESC')->first();
        if (!$request->input('amount')) {
            return redirect('/withdraw')->withInput()->with('error','Please input withdraw amount');
        } elseif ($request->input('amount') > $blc) {
            return redirect('/withdraw')->withInput()->with('error','Not enough balance');
        } elseif (!$request->input('pin')) {
            return redirect('/withdraw/info')->withInput()->with('error','Please input PIN');
        } elseif (!Hash::check($request->input('pin'),$pin->pin)) {
            return redirect('/withdraw/info')->withInput()->with('error','Wrong PIN');
        } else {
            DB::beginTransaction();
            try {
                // return redirect('/withdraw')->withInput()->with('error','Terjadi Kesalahan, coba beberapa saat lagi');
                // $user->tokocryptoKey              = $request->input('apikey');
                // $user->tokocryptoSecret           = $request->input('secretkey');
                // $user->save();
                // return redirect('/tokocrypto')->with('success','Data berhasil di simpan');
                $currency                               = Currency::where('active',1)->where('id',$request->input('code'))->first();
                $row                                    = NEW Transaction;
                $row->user_id                           = $user->id;
                $row->trx_id                           = 'W'.$user->id.date('ymdhis');;
                $row->amount                            = $request->input('amount');
                $row->type                              = 'withdraw';
                // $row->sender                            = $request->input('sender');
                // $row->receiver                          = $currency->address;
                $row->currency_id                       = $currency->id;
                $row->code                              = $currency->code;
                $row->bankID                            =$request->session()->get('bankID');
                $row->save();

                $lastBalance        = Balance::where('user_id',$user->id)->where('currency_id',$currency->id)->where('active',1)->orderBy('id','DESC')->first();
                if($lastBalance) {
                    $balance = $lastBalance->amount - $row->amount;
                } else {
                    $balance = 0;
                }

                $newBalance                             = NEW Balance;
                $newBalance->user_id                    = $row->user_id;
                $newBalance->currency_id                = $row->currency_id;
                $newBalance->code                       = $row->code;
                $newBalance->amount                     = $balance;
                $newBalance->save();

                $binance                                = NEW Binance;
                $send['url']                            = '/ticker/24hr';
                $body['symbol']                         = $currency->code;
                $send['body']                           = $body; 
                $post                                   = $binance->callApi($send);

                $amount                                 = $post->lastPrice * $request->input('amount');
                // $amount                                 = $currency->price * $request->input('amount');

                if($request->session()->get('type') == 'bank') {
                    $ipaymu                                 = NEW Ipaymu;
                    $send['url']                            = env('IPAYMU_URL').'/api/v2/withdrawbank';
                    $send['va']                             = env('IPAYMU_VA');
                    $send['secret']                         = env('IPAYMU_KEY');
                    $send['method']                         = 'POST';
                    $body['bankCode']                       = $request->session()->get('bankCode');
                    $body['bankName']                       = $request->session()->get('bankName');
                    $body['bankNumber']                     = $request->session()->get('bankNumber');
                    $body['bankAccount']                    = $request->session()->get('bankAccount');
                    $body['amount']                         = $amount;
                    $body['referenceId']                    = $row->id;
                    $body['notes']                          = 'Withdraw Lautan Trx ID'.$row->id;
                    $send['body']                           = json_encode($body, JSON_UNESCAPED_SLASHES);
                    $post                                   = $ipaymu->callApi($send);
                    if($post AND $post->Status == 200) {
                        $row->ipaymuId                      = $post->Data->TransactionId;
                        $row->save();
                        DB::commit();
                        return redirect('/withdraw')->with('success','Withdraw Success');
                    } else {
                        DB::rollback();
                        return redirect('/withdraw')->withInput()->with('error','Withdraw error, please try again later');
                    }
                }elseif($request->session()->get('type') == 'bankk'){
                    // $virtual_ac                  = "9920001189";
                    // $faspayKey                   = "c2b21b0e-e988-4fc8-ae31-39d7177a7186";
                    // $faspaySecret                = "19c887a3-49af-4087-8e82-58a1875b1452";
                    // $appKey                      = "055061d5-626a-420b-8e1a-dbd6772fd5f6";
                    // $appSecret                   = "60d5ce04-b077-44c5-b5b8-6ceeeab11e18";
                    // $clientKey                   = "571f97de-3216-4598-a80c-240e6d571000";
                    // $clientSecret                = "3155e4cb-58af-46c7-b1df-86cd54809672";
                    // $iv                          = "faspay2018xAuth@#";

                    $b_ac = db::table('user_bank')->where('id',$request->session()->get('bankID'))->first();
                    $sendme = new SendMe();	
                    $sendme->enableProd();
                    $reg = $sendme->transfer([	
                        // "virtual_account" => "9920001189",
                        "virtual_account" => "9847090569",
                        "beneficiary_virtual_account" => $b_ac->bva,
                        "beneficiary_account" =>$b_ac->account_number,
                        // "beneficiary_name" => "BCA Dummy",
                        "beneficiary_name" => $b_ac->ban,
                        "beneficiary_bank_code" => $b_ac->bank_code,
                        "beneficiary_region_code" => "0102",
                        "beneficiary_country_code" => "ID",
                        "beneficiary_purpose_code" => "1",
                        "beneficiary_email" => $user->email,
                        "trx_no" => $user->trx_fp,
                        "trx_date" => Date("Y-m-d H:i:s"),
                        "instruct_date" => "",
                        "trx_amount" => round($amount,0)*100,
                        "trx_desc" => "Withdrawal",
                        "callback_url" => "https://lautan.io/api/withdraw/sendme_notify"
                    ]);    
                        
                    // $reg = $sendme->mutasi(["start_date" => "2022-01-10", "end_date" => "2022-01-20"]);
                    
                    // return json_encode($reg['status']);
                    // return json_encode($reg);
                    // $data_notif = file_get_contents('php://input');
                    
                    // dd($data_notif);
                    // echo now();
                // echo $reg;
                // dd($sendme);
                
                
                        // return json_encode($reg);
                    if($reg['status'] == 1) {
                        // $row->ipaymuId                      = $post->Data->TransactionId;
                        // $row->save();
                        DB::commit();
                        DB::table('sendme')->insert([
                            'user_id' => $user->id,
                            'currency_code' => $request->input('code'),
                            'currency_amount' => $request->input('amount'),
                            'trx_id' => $reg['trx_id'],
                            'trx_no' => $reg['trx_no'],
                            'trx_date' => $reg['trx_date'],
                            'trx_amount' => round($amount,0),
                            'status' => $reg['status'],
                            'tid' => $row->id
                        ]);
                       
                        // return json_encode($reg);
                        return redirect('/withdraw')->with('success','Withdraw Success');
                        
                    }elseif(round($amount,0) < 10000){
                        DB::rollback();
                        return redirect('/withdraw')->withInput()->with('error','Withdraw error, amount below the minimum withdrawal. Amount must be more than Rp. 10.000');
                    }
                    else {
                        DB::rollback();
                        return redirect('/withdraw')->withInput()->with('error','Withdraw error, please try again later');
                    }
                    // dd($sendme);
                }elseif($request->session()->get('type') == 'idcash'){
                    // $va                         = env('IPAYMU_VA');
                    // $api                        = env('IPAYMU_KEY');
                    $va                         = $user->idcashVa;
                    $api                        = $user->idcashKey;
                    $integration_key            = Idcash::encryptQR($va . '|' . $api, env('IDCASH_KEY'));
                    // register to idcash ambil va dan key
                    $idcash                     = NEW Idcash;
                    $send['url']                = env('IDCASH_URL').'reload';
                    $send['method']             = 'POST';
                    $body['amount']             = $amount;
                    $body['request']            = 'reload';
                    $send['va']                 = $user->idcashVa;
                    $send['secret']             = $user->idcashKey;
                    $send['body']               = $body;
                    $post                       = $idcash->callApi($send);
                    dd($post);
                    if($post AND $post->Status == 201) {
                        // $row->ipaymuId          = $post->Data->TransactionId;
                        // $row->save();
                        DB::commit();
                        return redirect('/withdraw')->with('success','Withdraw Success');
                    } else {
                        DB::rollback();
                        return redirect('/withdraw')->withInput()->with('error','Withdraw error, please try again later');
                    }
                }
            } catch (ClientException $e) {
                DB::rollback();
                Log::info($e->getMessage());
                return redirect('/withdraw')->withInput()->with('error','Withdraw error, please try again later');
            }
        }
    }
    
    public function sendme_notify(Request $request){
        // $user_id 	='bot34465';
        // $passw		='p@ssw0rd';
        $user_id 	='bot34465';
        $passw		='gL@i9ckU';
        
        $token = csrf_token();
        // $data = array(
        //     "request"=> $request->request,
        //     "virtual_account"=> $request->virtual_account,
        //     "va_name"=> $request->va_name,
        //     "beneficiary_virtual_account"=> $request->beneficiary_virtual_account,
        //     "beneficiary_account"=> $request->beneficiary_account,
        //     "beneficiary_name"=> $request->beneficiary_name,
        //     "trx_id"=> $request->trx_id,
        //     "trx_date"=> $request->trx_date,
        //     "trx_amount"=> $request->trx_amount,
        //     "trx_desc"=> $request->trx_desc,
        //     "trx_status"=> $request->trx_status,
        //     "trx_status_date"=> $request->trx_status_date,
        //     "trx_reff"=> $request->trx_reff,
        //     "trx_no"=> $request->trx_no,
        //     "bank_code"=> $request->bank_code,
        //     "bank_name"=> $request->bank_name,
        //     "bank_response_code"=> $request->bank_response_code,
        //     "bank_response_msg"=> $request->bank_response_msg,
        // );
        
        // return response()->json(["message"=>$request->input('request')]);
        
$data =  '{
    "request": "'.$request->input("request").'",
    "virtual_account": "'.$request->virtual_account.'",
    "va_name": "'.$request->va_name.'",
    "beneficiary_virtual_account": "'.$request->beneficiary_virtual_account.'",
    "beneficiary_account": "'.$request->beneficiary_account.'",
    "beneficiary_name": "'.$request->beneficiary_name.'",
    "trx_id": "'.$request->trx_id.'",
    "trx_date": "'.$request->trx_date.'",
    "trx_amount": "'.$request->trx_amount.'",
    "trx_desc": "'.$request->trx_desc.'",
    "trx_status": "'.$request->trx_status.'",
    "trx_status_date": "'.$request->trx_status_date.'",
    "trx_reff": "'.$request->trx_reff.'",
    "trx_no": "'.$request->trx_no.'",
    "bank_code": "'.$request->bank_code.'",
    "bank_name": "'.$request->bank_name.'",
    "bank_response_code": "'.$request->bank_response_code.'",
    "bank_response_msg": "'.$request->bank_response_msg.'"
}';

// $data =  '{ 
//     "request":"Notification",
//     "virtual_account":"9920001189",
//     "va_name":"Sandbox 3",
//     "beneficiary_virtual_account":"9920020792",
//     "beneficiary_account":"1060045760",
//     "beneficiary_name":"BCA Dummy",
//     "trx_id":"591289",
//     "trx_date":"2022-01-26 14:56:53",
//     "trx_amount":"5333248",
//     "trx_desc":"Withdrawal",
//     "trx_status":"2",
//     "trx_status_date":"2022-01-26 14:57:24.923868",
//     "trx_reff":"xLRSR7RpWT-W7a0Uo7AT_1643183826577244",
//     "trx_no":"260054",
//     "bank_code":"014",
//     "bank_name":"BANK BCA",
//     "bank_response_code":"2",
//     "bank_response_msg":"Transaksi Berhasil"
// }';
        // $data_notif = file_get_contents('php://input');
        
        // $data = json_decode($data_notif);
        
        // echo "tes";
        // dd($data_notif);
        //signature
        
// $param["production"]["virtual_account"] 	= "9847090569";
// $param["production"]["faspay_key"]			= "c2b21b0e-e988-4fc8-ae31-39d7177a7186";
// $param["production"]["faspay_secret"] 		= "19c887a3-49af-4087-8e82-58a1875b1452";
// $param["production"]["app_key"]				= "6e7a0e43-6b83-4891-8f7b-b534194c0cfa";
// $param["production"]["app_secret"]			= "84dce914-e5e2-441e-9f3d-68febf898280";
// $param["production"]["client_key"]			= "d563f2bb-a901-4b81-8fd3-305770b284a8";
// $param["production"]["client_secret"] 		= "c0869c7e-1e16-4177-b48c-0f1db9540c9e";
// $param["production"]["iv"]					= "faspay2018xAuth@#";

//prod
        $client_id = "d563f2bb-a901-4b81-8fd3-305770b284a8";
        $client_secret = "c0869c7e-1e16-4177-b48c-0f1db9540c9e";
        $app_key = "6e7a0e43-6b83-4891-8f7b-b534194c0cfa";
        $app_secret = "84dce914-e5e2-441e-9f3d-68febf898280";

//dev
//  $client_id = "571f97de-3216-4598-a80c-240e6d571000";
//         $client_secret = "3155e4cb-58af-46c7-b1df-86cd54809672";
//         $app_key = "055061d5-626a-420b-8e1a-dbd6772fd5f6";
//         $app_secret = "60d5ce04-b077-44c5-b5b8-6ceeeab11e18";
        
        
        $strReplace = trim(preg_replace("/\r|\n|\t|\ /", "", $data));
        $requestBody = hash("sha256", $strReplace);
        
        $datas[] = $app_key;
        $datas[] = "POST";
        $datas[] = base64_encode($client_id.":".$client_secret);
        $datas[] = strtoupper($requestBody);
        
        $strToSign = implode(":", $datas);
        // print_r($strToSign);
        // exit();
        $signature = hash_hmac("sha256", $strToSign, $app_secret);
        // $signature = sha1(md5(($user_id.$passw.$data->bill_no.$data->payment_status_code)));
        
        // return response()->json(["message"=>$signature]);
        
        // ======= Make Signature Validation ===== 
        if ($request->input('signature') == $signature) {
        
        	/* ====== Update Transaction Status ==== */
        	if ($request->bank_response_code == '2'){
        
        		/* === update your transaction status === */
                 
                $sm = DB::table('sendme')->where("trx_no", $request->trx_no)->first();
                // $sm = DB::select('SELECT * FROM sendme WHERE trx_no = "'.$request->trx_no.'"');
                // return response()->json(["message"=>$sm->id]);
                
                        
                        DB::beginTransaction();
                        try {
                        
                                
                        //         // $send = DB::table('sendme')->where('trx_no',$request->trx_no)->first();
                                
                                $currency                               = Currency::where('active',1)->where('id',$sm->currency_code)->first();
                                $row                                    = Transaction::where('id', $sm->tid)->first();
                                $row->status                            = '1';
                                $row->save();
                                // return response()->json(["message"=>$currency]);
                        //         // DB::table('transactions')->where('id','=',$sm->tid)
                        //         // ->update([
                        //         //     'status' => '1'
                        //         // ]);
                                
                                
                                // $lastBalance        = Balance::where('user_id',$sm->user_id)->where('currency_id',$currency->id)->where('active',1)->orderBy('id','DESC')->first();
                                // if($lastBalance) {
                                //     $balance = $lastBalance->amount - $row->amount;
                                // } else {
                                //     $balance = 0;
                                // }
                
                                // $newBalance                             = NEW Balance;
                                // $newBalance->user_id                    = $row->user_id;
                                // $newBalance->currency_id                = $row->currency_id;
                                // $newBalance->code                       = $row->code;
                                // $newBalance->amount                     = $balance;
                                // $newBalance->save();
                                
                                DB::table('sendme')->where('trx_no',$request->trx_no)
                                ->update([
                                    'status' => $request->bank_response_code
                                ]);
                                
                                
                                DB::commit();
                    //         	$data = array( 
                				// "response"=>"Notification",
                    //           "virtual_account"=>$data->virtual_account,
                    //           "beneficiary_virtual_account"=>$data->beneficiary_virtual_account,
                    //           "bank_code"=>$data->bank_code,
                    //           "bank_name"=>$data->bank_name,
                    //           "response_code"=>"00",
                    //           "response_desc"=>"Success"
                    //     		);
                        
                        // 		$response = json_decode($data);
                        
                        // 		echo $response;
                        
                                $user = db::table('users')->where('id',$sm->user_id)->first();
                        		
                        		 $data = [
                                        'name'  => $user->name,
                                        'amount' => $request->trx_amount,
                                        'fee' => 0,
                                        'remain' => $request->trx_amount - 0,
                                        'bname' => $request->bank_name,
                                        'bacc' => $request->beneficiary_account,
                                        'time' => $request->trx_date,
                                        'trx' => $row->trx_id
                                        
                                    ];
                        
                                    Mail::send('emails.withdrawok', $data, function($message)use($data, $user) {
                                        $message->to($user->email)
                                                ->subject('Withdraw Success');
                                    });
                        
                                return response()->json([
                                    	"response"=>"Notification",
                                      "virtual_account"=>$request->virtual_account,
                                      "beneficiary_virtual_account"=>$request->beneficiary_virtual_account,
                                      "bank_code"=>$request->bank_code,
                                      "bank_name"=>$request->bank_name,
                                      "response_code"=>"00",
                                      "response_desc"=>"Success"]);
                                
                               
                		
                        } catch (ClientException $e) {
                            DB::rollback();
                            Log::info($e->getMessage());
                            return redirect('/withdraw')->withInput()->with('error','Withdraw error, please try again later');
                        }
        		
               
        
        		/* === give response to faspay === */
        	
        	}else{
         return response()->json(["message"=>"Bank Response Invalid"]);
        }
        	
        }else{
         return response()->json(["message"=>"Signature Not match"]);
        }
    }

    private function encryptAES256($string, $key){
		$plaintext 	= $string;
		$method 	= "aes-256-cbc";
		$password 	= substr(hash('sha256', $key, true), 0, 32);            
		$iv 		= substr(md5($key."faspay2018xAuth@#"), -16);
		
		return base64_encode(openssl_encrypt($plaintext, $method, $password, OPENSSL_RAW_DATA, $iv));
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function bank(){
        return view('withdraw.bankacc');
    }
    public function add_bank(Request $request){
        DB::table('user_bank')->insert([
            'user_id' => Auth::user()->id,
                            'bank_code' => $request->bank_code,
                            'bank_account' => $request->bank_account,
                            'account_number' => $request->account_number,
                            'isReg' => 0
                        ]);
        return redirect('/withdraw')->withInput()->with('success','Add bank account success!!');
    }
    public function edit_bank(Request $request,$id){
        DB::table('user_bank')->where('id',$id)->update([
                            'bank_code' => $request->bank_code,
                            'bank_account' => $request->bank_account,
                            'account_number' => $request->account_number,
                            'isReg' => 0
                        ]);
        return redirect('/withdraw')->withInput()->with('success','Edit bank account success!!');
    }
    
    public function del_bank($id){
        
        $b = DB::table('user_bank')->where('id',$id)->delete();
        return redirect('/withdraw')->withInput()->with('success','Bank account success deleted!!');
    }
    
    public function testemail(Request $request){
            
            $user = db::table('users')->where('email','risfandibayu19@gmail.com')->first();
                
            $data = [
                'name'  => $user->name, 
            ];

            Mail::send('emails.withdrawreqrej', $data, function($message)use($data, $user) {
                $message->to($user->email)
                        ->subject('Withdraw Success');
            });
            
            dd(date('ymdHis'));
            // echo "tes";
    }
}
