<?php

namespace App\Http\Controllers;

use App\Libs\UploadImage;
use App\Models\custom_variabel;
use Illuminate\Support\Str;
use App\Models\Webmedia;
use App\Models\Webpages;
use Illuminate\Http\Request;

class WebadminController extends Controller
{
    public function listMedia(Request $request)
    {
        $menu = 'webadmin';
        $data = Webmedia::orderBy('id','DESC')->get();
        return view('webadmin.media', compact('menu','data'));
    }

    public function uploadMedia(Request $request)
    {
        $row    = NEW Webmedia();

        if($request->hasFile('files')) 
        {
            $fileProfile        = $request->file('files');
            $uploadImage        = New UploadImage;
            $filenameProfile    = 'media_' . Str::random(6) . '.' . $fileProfile->getClientOriginalExtension();
            $filepathProfile    = 'webcontent/' . $filenameProfile;
            $uploadProfile      = $uploadImage->upload($filepathProfile, $fileProfile);
            $row->images        = $uploadProfile;
            $row->url           = $uploadProfile;
        }
        $row->save();

        return redirect('/webadmin/media')->with('success','Data has been add');
    }

    public function listPages(Request $request)
    {
        $menu = 'webadmin';
        $data = Webpages::orderBy('id','ASC')->get();
        return view('webadmin.pages', compact('menu','data'));
    }

    public function addPages(Request $request)
    {
        $row            = NEW Webpages();
        $row->slug      = $request->slug;
        $row->title     = $request->title;
        $row->content   = $request->content;
        $row->status    = $request->status;
        $row->save();

        return redirect('/webadmin/pages')->with('success','Data has been add');
    }

    public function detailPages($id)
    {
        $menu = 'webadmin';
        $data = Webpages::where('id', $id)->first();
        return view('webadmin.pagesedit', compact('menu','data'));
    }

    public function editPages(Request $request)
    {
        $row = Webpages::where('id', $request->id)->first();
        $row->slug      = $request->slug;
        $row->title     = $request->title;
        $row->content   = $request->content;
        $row->status    = $request->status;
        $row->save();

        return redirect('/webadmin/pages')->with('success','Data has been update');
    }

    public function noactivePages($id)
    {
        $row = Webpages::where('id', $id)->first();
        $row->status    = 0;
        $row->save();
        return redirect('/webadmin/pages')->with('success','Data has been update');
    }

    public function activePages($id)
    {
        $row = Webpages::where('id', $id)->first();
        $row->status    = 1;
        $row->save();
        return redirect('/webadmin/pages')->with('success','Data has been update');
    }

    public function customvar(Request $request)
    {
        $menu = 'webadmin';
        $data = custom_variabel::orderBy('id','ASC')->get();
        return view('webadmin.customvar', compact('menu','data'));
    }

    public function addCustomvar(Request $request)
    {
        $row = NEW custom_variabel();

        $row->type  = $request->type;
        $row->code  = $request->code;
        $row->value = $request->value;
        $row->save();

        return redirect('/webadmin/customvar')->with('success','Data has been add');
    }

    public function detailCustomvar($id)
    {
        $menu = 'webadmin';
        $data = custom_variabel::where('id', $id)->first();
        return view('webadmin.customvaredit', compact('menu','data'));
    }

    public function editCustomvar(Request $request)
    {
        $row = custom_variabel::where('id', $request->id)->first();
        $row->type  = $request->type;
        $row->code  = $request->code;
        $row->value = $request->value;
        $row->save();

        return redirect('/webadmin/customvar')->with('success','Data has been update');
    }

    
}
