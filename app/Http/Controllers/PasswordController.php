<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Hash;

class PasswordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu = 'password';
        return view('account.password')->with(compact('menu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        if (!$request->input('oldpassword')) {
            return redirect('/password')->withInput()->with('error','Masukan password lama');
        } else if (!$request->input('password')) {
            return redirect('/password')->withInput()->with('error','Masukan password baru');
        } else if (!$request->input('repassword')) {
            return redirect('/password')->withInput()->with('error','Ulangi password baru');
        } else if(!Hash::check($request->input('oldpassword'),$user->password)) {
            return redirect('/password')->withInput()->with('error','Password lama salah');
        } else if ($request->input('oldpassword') != $request->input('repassword')) {
            return redirect('/password')->withInput()->with('error','Password dan ulangi password tidak sama');
        } else {
            $user->password              = Hash::make($request->input('password'));
            $user->save();
            return redirect('/password')->with('success','Password berhasil di ubah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
