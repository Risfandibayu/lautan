<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use Hash;
use App\Models\User;
use Mail;
use Uuid;
use DB;
use Log;
class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('register');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->input('name')) {
            return redirect('/register')->withInput()->with('error','Please input your name');
        } elseif (!$request->input('email') or !filter_var($request->input('email'), FILTER_VALIDATE_EMAIL)) {
            return redirect('/register')->withInput()->with('error','Please input a valid email');
        } elseif (User::where('email', strtolower($request->input('email')))->first()) {
            return redirect('/register')->withInput()->with('error','Email already registered');
        } elseif (!$request->input('phone') or !is_numeric($request->input('phone'))) {
            return redirect('/register')->withInput()->with('error','Please enter a valid phone number');
        // } elseif (User::where('phone', $request->input('phone'))->first()) {
            // return redirect('/register')->withInput()->with('error','Phone number already registered');
        } elseif (!$request->input('password')) {
            return redirect('/register')->withInput()->with('error','Please input your password');
        } elseif (!$request->input('repassword')) {
            return redirect('/register')->withInput()->with('error','Please input your re-password');
        } elseif ($request->input('password') != $request->input('repassword')) {
            return redirect('/register')->withInput()->with('error','Password and re-password not match');
        } else {
            DB::beginTransaction();
            try {
                $row            = NEW User;
                $row->name      = $request->input('name');
                $row->email     = strtolower($request->input('email'));
                $row->phonecode = $request->input('phonecode');
                $row->phone     = $request->input('phone');
                $row->password  = Hash::make($request->input('password'));
                $row->uuid      = strtoupper(Uuid::generate(4));
                $row->save();

                $data = [
                    'name' => $row->name,
                    'url' => url('register/'.$row->uuid)
                ];

                Mail::send('emails.register', $data, function($message)use($data, $row) {
                    $message->to($row->email)
                            ->subject('Konfirmasi Pendaftaran');
                });
                DB::commit();
                return redirect('/login')->with('success','Registrasi berhasil, silahkan cek email Anda');
            } catch (ClientException $e) {
                DB::rollback();
                return redirect('/register')->with('error','Terjadi kesalahan');
            }
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($row = User::where('uuid',$id)->where('active',1)->first()) {
            Auth::login($row);
            return redirect('/dashboard');
        } else {
            return redirect('/login')->with('error','User tidak ditemukan');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
