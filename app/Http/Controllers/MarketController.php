<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libs\Binance;
use App\Models\Currency;
class MarketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu               = 'market';
        $currency           = Currency::where('active',1)->orderBy('name')->get();
        $coin               = [];
        foreach ($currency as $rows) {
            $binance                                = NEW Binance;
            $send['url']                            = '/ticker/24hr';
            $body['symbol']                         = $rows->code;
            $send['body']                           = $body; 
            $post                                   = $binance->callApi($send);
            if($post) {
                $list['id']                         = $rows->id;
                $list['image']                      = $rows->image;
                $list['code']                       = $rows->code;
                $list['name']                       = $rows->name;
                $list['price']                      = $post->lastPrice;
                $list['change']                     = $post->priceChangePercent;
                $list['highPrice']                  = $post->highPrice;
                $list['lowPrice']                   = $post->lowPrice;
                $list['marketPrice']                = $post->quoteVolume;
                $coin[]                             = $list;
            }   
        }
        $row                                        = json_decode(json_encode($coin), FALSE);
        // dd($row);
        return view('market.index')->with(compact('menu','row'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
