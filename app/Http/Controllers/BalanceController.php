<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Hash;
use App\Models\Currency;
class BalanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $menu = 'account';
        // return view('account.tokocrypto')->with(compact('menu'));
        // if(!Auth::user()->tokocryptoKey OR !Auth::user()->tokocryptoSecret) {
        //     return redirect('/tokocrypto')->with('warning','Silahkan hubungkan akun Tokocrypto Anda terlebih dahulu sebelum dapat mengakses menu Saldo');
        // } else {
            $menu = 'balance';
            $row                = Currency::where('active',1)->orderBy('name')->get();
            return view('balance.index')->with(compact('menu','row'));
        // }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        if (!$request->input('apikey')) {
            return redirect('/tokocrypto')->withInput()->with('error','Masukan api key');
        } else if (!$request->input('secretkey')) {
            return redirect('/tokocrypto')->withInput()->with('error','Masukan secret key');
        } else {
            $user->tokocryptoKey              = $request->input('apikey');
            $user->tokocryptoSecret           = $request->input('secretkey');
            $user->save();
            return redirect('/tokocrypto')->with('success','Data berhasil di simpan');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
