<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Datalog;
class DatalogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->session()->forget('kk_search');
        $request->session()->forget('kk_search_status');
        $request->session()->forget('kk_start_date');
        $request->session()->forget('kk_end_date');
        $request->session()->forget('kk_type');
        
        $menu               = 'datalog';
        $title              = 'Log Data';
        $row                = Datalog::orderBy('id','DESC')->paginate(8);
        return view('datalog.index')->with(compact('menu','row','title'));
    }

    public function postSearch(Request $request)
    {
        $request->session()->put('kk_search', $request->input('search'));
        $request->session()->put('kk_start_date', $request->input('start_date'));
        $request->session()->put('kk_end_date', $request->input('end_date'));
        $request->session()->put('kk_type', $request->input('type'));
        $request->session()->put('kk_search_status', true);
        return redirect('/datalog/search');
    }

    public function getSearch(Request $request)
    {
        $key            = $request->session()->get('kk_search');
        $status         = $request->session()->get('kk_status');
        $startdate      = $request->session()->get('kk_start_date');
        $enddate        = date('Y-m-d', strtotime($request->session()->get('kk_end_date') . ' + 1 days'));
        //$enddate        = $request->session()->get('kk_end_date');
        $type           = $request->session()->get('kk_type');
        $menu           = 'datalog';
        $title          = 'Log Data';
        $student        = Datalog::
                        when($key != '', function($qkey) use ($key) {
                            return $qkey->whereRaw("log LIKE '%".$key."%'");
                        })
                        ->when($type != '', function($qtype) use ($type) {
                            return $qtype->where('channel', $type);
                        })
                        ->when($startdate != '', function($qstartdate) use ($startdate,$enddate) {
                            return $qstartdate->whereDate('updated_at', '>=', $startdate)
                                    ->when($enddate != '', function($qenddate) use ($enddate) {
                                        return $qenddate->whereDate('updated_at', '<', $enddate);
                                    });
                        });
        $row            = $student->orderBy('id','DESC')->paginate(8);
        return view('datalog.index')->with(compact('menu','row','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$user               = Auth::user();
        $menu               = 'datalog';
        $title              = 'Log Data';
        $row                = Datalog::where('id',$id)->first();
        if($row) {
            //$university         = University::where('active',1)->orderBy('name')->get();
            return view('datalog.show')->with(compact('menu','row','title'));
        } else {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
