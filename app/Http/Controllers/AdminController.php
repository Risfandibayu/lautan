<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use Hash;
use App\Models\User;
use App\Models\Bank;
use DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu               = 'admin';
        $row                = User::where('active',1)->where('type','admin')->orderBy('name')->get();
        return view('admin.index')->with(compact('menu','row'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menu               = 'admin';
        return view('admin.create')->with(compact('menu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $lid = User::max('id');
        $id = $lid + 1;
        if(!$request->input('name')) {
            return redirect('/admin/create')->withInput()->with('error','Please input name');
        } elseif (!$request->input('email') or !filter_var($request->input('email'), FILTER_VALIDATE_EMAIL)) {
            return redirect('/admin/create')->withInput()->with('error','Please input a valid email');
        } elseif (User::where('email', strtolower($request->input('email')))->where('id','!=',$id)->where('active',1)->where('type','admin')->first()) {
            return redirect('/admin/create')->withInput()->with('error','Email already registered');
        } elseif (!$request->input('phone') or !is_numeric($request->input('phone'))) {
            return redirect('/admin/create')->withInput()->with('error','Please enter a valid phone number');
        } elseif (User::where('phone', $request->input('phone'))->where('id','!=',$id)->where('active',1)->where('type','admin')->first()) {
            return redirect('/admin/create')->withInput()->with('error','Phone number already registered');
        } else {
            $row                        = NEW User;
            $row->type                  = 'admin';
            $row->name                  = $request->input('name');
            $row->email                 = strtolower($request->input('email'));
            $row->phone                 = $request->input('phone');
            if($request->input('password')) {
                $row->password  = Hash::make($request->input('password'));
            }
            $row->save();

            return redirect('/admin')->with('success','Data has been update');
        }
        // dd($id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu               = 'admin';
        $row                = User::where('active',1)->where('id',$id)->first();
        if($row) {
            return view('admin.edit')->with(compact('menu','row'));
        } else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $row                = User::where('active',1)->where('id',$id)->first();
        if($row) {
            if(!$request->input('name')) {
                return redirect('/admin/'.$id.'/edit')->withInput()->with('error','Please input name');
            } elseif (!$request->input('email') or !filter_var($request->input('email'), FILTER_VALIDATE_EMAIL)) {
                return redirect('/admin/'.$id.'/edit')->withInput()->with('error','Please input a valid email');
            } elseif (User::where('email', strtolower($request->input('email')))->where('id','!=',$id)->where('active',1)->where('type','admin')->first()) {
                return redirect('/admin/'.$id.'/edit')->withInput()->with('error','Email already registered');
            } elseif (!$request->input('phone') or !is_numeric($request->input('phone'))) {
                return redirect('/admin/'.$id.'/edit')->withInput()->with('error','Please enter a valid phone number');
            } elseif (User::where('phone', $request->input('phone'))->where('id','!=',$id)->where('active',1)->where('type','admin')->first()) {
                return redirect('/admin/'.$id.'/edit')->withInput()->with('error','Phone number already registered');
            } else {
                $row->name                  = $request->input('name');
                $row->email                 = strtolower($request->input('email'));
                $row->phone                 = $request->input('phone');
                if($request->input('password')) {
                    $row->password  = Hash::make($request->input('password'));
                }
                $row->save();

                return redirect('/admin')->with('success','Data has been update');
            }
        } else {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        // $id                 = $request->input('id');
        $row                = User::where('active',1)->where('id',$id)->first();
        if($row) {
            $row->active    = 0;
            $row->save();
            return redirect('/admin')->with('success', 'Admin has been deactivate');
        } else {
            abort(404);
        }
    }
}
