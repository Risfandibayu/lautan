<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use Hash;
use App\Models\AgentRole;

class AgentRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu               = 'agent';
        $row                = AgentRole::where('active',1)->orderBy('level')->get();
        return view('agentrole.index')->with(compact('menu','row'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menu               = 'agent';
        return view('agentrole.create')->with(compact('menu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!$request->input('name')) {
            return redirect('/agentrole/'.$id.'/edit')->withInput()->with('error','Please input role name');
        } elseif (!$request->input('fees') or !is_numeric($request->input('fees'))) {
            return redirect('/agentrole/'.$id.'/edit')->withInput()->with('error','Please enter a valid fees number');
        } else {
            $row                        = NEW AgentRole;
            $row->name                  = $request->input('name');
            $row->fees                  = $request->input('fees');
            $row->save();

            $row->level                 = $row->id;
            $row->save();

            return redirect('/agentrole')->with('success','Data has been update');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu               = 'agent';
        $row                = AgentRole::where('active',1)->where('id',$id)->first();
        if($row) {
            return view('agentrole.edit')->with(compact('menu','row'));
        } else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $row                = AgentRole::where('active',1)->where('id',$id)->first();
        if($row) {
            if(!$request->input('name')) {
                return redirect('/agentrole/'.$id.'/edit')->withInput()->with('error','Please input role name');
            } elseif (!$request->input('fees') or !is_numeric($request->input('fees'))) {
                return redirect('/agentrole/'.$id.'/edit')->withInput()->with('error','Please enter a valid fees number');
            } else {
                $row->name                  = $request->input('name');
                $row->fees                  = $request->input('fees');
                $row->save();

                return redirect('/agentrole')->with('success','Data has been update');
            }
        } else {
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $id                 = $request->input('id');
        $row                = AgentRole::where('active',1)->where('id',$id)->first();
        if($row) {
            $row->active    = 0;
            $row->save();
            return redirect('/agentrole')->with('success', 'Admin has been deactivate');
        } else {
            abort(404);
        }
    }
}
