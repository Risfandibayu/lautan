<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AgentRole;
class AgentRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role           = NEW AgentRole;
        $role->name     = 'Agent Director';
        $role->fees     = 4;
        $role->level    = 1;
        $role->save();

        $role           = NEW AgentRole;
        $role->name     = 'Agent Manager';
        $role->fees     = 8;
        $role->level    = 2;
        $role->save();

        $role           = NEW AgentRole;
        $role->name     = 'Agent';
        $role->fees     = 28;
        $role->level    = 3;
        $role->save();
    }
}
