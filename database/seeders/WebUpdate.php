<?php

namespace Database\Seeders;

use App\Models\custom_variabel;
use App\Models\User;
use App\Models\Webpages;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class WebUpdate extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user           = NEW User;
        $user->type     = 'admin';
        $user->name     = 'Admin';
        $user->email    = 'agokeren@gmail.com';
        $user->phone    = '087752899147';
        $user->password = Hash::make('damayanti46#');
        $user->save();

        $customvar          = NEW custom_variabel;
        $customvar->type    = 'web';
        $customvar->code    = 'comingsoon';
        $customvar->value   = 'on';
        $customvar->save();

        $web            = NEW Webpages;
        $web->title     = '#header#';
        $web->slug      = '#header#';
        $web->content   = '<style>
        .nopadding {padding: 0 !important;margin: 0 !important;}
        .btn { border-radius:3 !important; }
        .form-control { border-radius:3 !important; border-color:#aaaaaa }
        .select2-selection__rendered {line-height: 35px !important;}
        .select2-container .select2-selection--single {height: 38px !important; }
        .select2-selection__arrow { height: 36px !important; }
        </style>
        </head>
        
        
        <!-- page wrapper -->
        <body class="boxed_wrapper">
        <!-- main header -->
        <header class="main-header style-three">
            <div class="header-top">
                <div class="container">
                    <div class="inner-container clearfix">
                        <ul class="header-info pull-left">
                            <li><i class="fas fa-phone-volume"></i><a href="tel:0092155596">009-215-5596</a></li>
                            <li><i class="far fa-envelope"></i><a href="mailto:info@domain.com">info@domain.com</a></li>
                        </ul>
                        <ul class="header-nav pull-right">
                            <li><a href="#">Login</a></li>
                            <li><a href="#">Support</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="header-upper">
                <div class="outer-container">
                    <div class="container">
                        <div class="main-box clearfix">
                            <div class="logo-box pull-left">
                                <figure class="logo"><a href="index.html"><img src="/landing-assets/images/logo.png" style="height:40px" alt=""></a></figure>
                            </div>
                            <div class="menu-area pull-right clearfix">
                                <!--Mobile Navigation Toggler-->
                                <div class="mobile-nav-toggler">
                                    <i class="icon-bar"></i>
                                    <i class="icon-bar"></i>
                                    <i class="icon-bar"></i>
                                </div>
                                <nav class="main-menu navbar-expand-md navbar-light">
                                    <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                        <ul class="navigation clearfix">
                                            <li><a href="/">Home</a></li> 
                                            <li><a href="/page/aboutus">About Us</a></li> 
                                            <li><a href="/page/product">Products</a></li>
                                            <li><a href="/page/info">Info</a></li>
                                            <li><a href="/page/partnership">Partnership</a></li>    
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            <!--sticky Header-->
            <div class="sticky-header">
                <div class="container clearfix">
                    <figure class="logo-box"><a href="index.html"><img src="/landing-assets/images/logo.png" style="height:40px" alt=""></a></figure>
                    <div class="menu-area">
                        <nav class="main-menu clearfix">
                            <!--Keep This Empty / Menu will come through Javascript-->
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <!-- main-header end -->
        
        <!-- Mobile Menu  -->
        <div class="mobile-menu">
            <div class="menu-backdrop"></div>
            <div class="close-btn"><i class="fas fa-times"></i></div>
            
            <nav class="menu-box">
                <div class="nav-logo"><a href="index.html"><img src="/landing-assets/images/logo.png" style="height:40px" alt="" title=""></a></div>
                <div class="menu-outer"><!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header--></div>
                <div class="contact-info">
                    <h4>Contact Info</h4>
                    <ul>
                        <li>Chicago 12, Melborne City, USA</li>
                        <li><a href="tel:+8801682648101">+88 01682648101</a></li>
                        <li><a href="mailto:info@example.com">info@example.com</a></li>
                    </ul>
                </div>
                <div class="social-links">
                    <ul class="clearfix">
                        <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                        <li><a href="#"><span class="fab fa-facebook-square"></span></a></li>
                        <li><a href="#"><span class="fab fa-pinterest-p"></span></a></li>
                        <li><a href="#"><span class="fab fa-instagram"></span></a></li>
                        <li><a href="#"><span class="fab fa-youtube"></span></a></li>
                    </ul>
                </div>
            </nav>
        </div><!-- End Mobile Menu -->';
        $web->save();

        $web            = NEW Webpages;
        $web->title     = '#headerpage#';
        $web->slug      = '#headerpage#';
        $web->content   = '<style>
        .nopadding {padding: 0 !important;margin: 0 !important;}
        .btn { border-radius:3 !important; }
        .form-control { border-radius:3 !important; border-color:#aaaaaa }
        .select2-selection__rendered {line-height: 35px !important;}
        .select2-container .select2-selection--single {height: 38px !important; }
        .select2-selection__arrow { height: 36px !important; }
        </style>
        </head>
        
        <!-- page wrapper -->
        <body class="boxed_wrapper">
        <!-- main header -->
        <header class="main-header style-three">
            <div class="header-top">
                <div class="container">
                    <div class="inner-container clearfix">
                        <ul class="header-info pull-left">
                            <li><i class="fas fa-phone-volume"></i><a href="tel:0092155596">009-215-5596</a></li>
                            <li><i class="far fa-envelope"></i><a href="mailto:info@domain.com">info@domain.com</a></li>
                        </ul>
                        <ul class="header-nav pull-right">
                            <li><a href="#">Login</a></li>
                            <li><a href="#">Support</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="header-upper">
                <div class="outer-container">
                    <div class="container">
                        <div class="main-box clearfix">
                            <div class="logo-box pull-left">
                                <figure class="logo"><a href="index.html"><img src="/landing-assets/images/logo.png" style="height:40px" alt=""></a></figure>
                            </div>
                            <div class="menu-area pull-right clearfix">
                                <!--Mobile Navigation Toggler-->
                                <div class="mobile-nav-toggler">
                                    <i class="icon-bar"></i>
                                    <i class="icon-bar"></i>
                                    <i class="icon-bar"></i>
                                </div>
                                <nav class="main-menu navbar-expand-md navbar-light">
                                    <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                        <ul class="navigation clearfix">
                                            <li><a href="/">Home</a></li> 
                                            <li><a href="/page/aboutus">About Us</a></li> 
                                            <li><a href="/page/product">Products</a></li>
                                            <li><a href="/page/info">Info</a></li>
                                            <li><a href="/page/partnership">Partnership</a></li>    
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            <!--sticky Header-->
            <div class="sticky-header">
                <div class="container clearfix">
                    <figure class="logo-box"><a href="index.html"><img src="/landing-assets/images/logo.png" style="height:40px" alt=""></a></figure>
                    <div class="menu-area">
                        <nav class="main-menu clearfix">
                            <!--Keep This Empty / Menu will come through Javascript-->
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <!-- main-header end -->
        
        <!-- Mobile Menu  -->
        <div class="mobile-menu">
            <div class="menu-backdrop"></div>
            <div class="close-btn"><i class="fas fa-times"></i></div>
            
            <nav class="menu-box">
                <div class="nav-logo"><a href="index.html"><img src="/landing-assets/images/logo.png" style="height:40px" alt="" title=""></a></div>
                <div class="menu-outer"><!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header--></div>
                <div class="contact-info">
                    <h4>Contact Info</h4>
                    <ul>
                        <li>Chicago 12, Melborne City, USA</li>
                        <li><a href="tel:+8801682648101">+88 01682648101</a></li>
                        <li><a href="mailto:info@example.com">info@example.com</a></li>
                    </ul>
                </div>
                <div class="social-links">
                    <ul class="clearfix">
                        <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                        <li><a href="#"><span class="fab fa-facebook-square"></span></a></li>
                        <li><a href="#"><span class="fab fa-pinterest-p"></span></a></li>
                        <li><a href="#"><span class="fab fa-instagram"></span></a></li>
                        <li><a href="#"><span class="fab fa-youtube"></span></a></li>
                    </ul>
                </div>
            </nav>
        </div><!-- End Mobile Menu -->';
        $web->save();

        $web            = NEW Webpages;
        $web->title     = '#footer#';
        $web->slug      = '#footer#';
        $web->content   = '<!-- main-footer -->
        <footer class="main-footer style-five">
            <div class="anim-icons">
                <div class="icon icon-1"><img src="/appway/images/icons/pattern-21.png" alt=""></div>
            </div>
            <div class="image-layer" style="background-image: url(/appway/images/icons/footer-bg-5.png);"></div>
            <div class="footer-top">
                <div class="container">
                    <div class="widget-section">
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-sm-12 footer-column">
                                <div class="about-widget footer-widget">
                                    <figure class="footer-logo"><a href="index.html"><img src="/landing-assets/images/logo.png" style="height:40px" alt=""></a></figure>
                                    <div class="text">Lorem ipsum dolor sit consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </div>
                                    <h2 class="phone"><i class="fas fa-phone-volume"></i><a href="tel:5184575182">518 - 457 - 5182</a></h2>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-6 col-sm-12 footer-column">
                                <div class="links-widget footer-widget">
                                    <h4 class="widget-title">Services</h4>
                                    <div class="widget-content">
                                        <ul class="list clearfix">
                                            <li><a href="#">Business Dashboards</a></li>
                                            <li><a href="#">Sales Analytics</a></li>
                                            <li><a href="#">Digital Marketing</a></li>
                                            <li><a href="#">Financial Help</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                                <div class="contact-widget footer-widget">
                                    <h4 class="widget-title">Contact Us</h4>
                                    <div class="widget-content">
                                        <ul class="contact-info clearfix">
                                            <li><i class="fas fa-map-marker-alt"></i> 25 Bedford St. New York City.</li>
                                            <li><i class="fas fa-phone"></i><a href="tel:0665184575181">(+066) 518 - 457 - 5181</a></li>
                                            <li><i class="fas fa-envelope"></i><a href="mailto:info@example.com">info@example.com</a></li>
                                        </ul>
                                    </div>
                                    <ul class="social-links clearfix">
                                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-skype"></i></a></li>
                                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                                <div class="links-widget footer-widget">
                                    <h4 class="widget-title">About Company</h4>
                                    <div class="widget-content">
                                        <ul class="list clearfix">
                                            <li><a href="#">Appway Online</a></li>
                                            <li><a href="#">Our Leadership</a></li>
                                            <li><a href="#">Carrers</a></li>
                                            <li><a href="#">What We Do</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container clearfix">
                    <div class="copyright pull-left">&copy; 2021 <a href="#">Lautan.io</a>. All rights reserved</div>
                    <ul class="footer-nav pull-right">
                        <li><a href="/page/terms-of-service">Terms of Service</a></li>
                        <li><a href="/page/privacy-policy">Privacy Policy</a></li>
                        <li><a href="/page/user-agreement">User Agreement</a></li>
                    </ul>
                </div>
            </div>
        </footer>
        <!-- main-footer end -->';
        $web->save();

        $web            = NEW Webpages;
        $web->title     = 'homepage';
        $web->slug      = 'homepage';
        $web->content   = '<!-- increase-website -->
        <section class="increase-website">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                        <div id="content_block_44">
                            <div class="content-box">
                                <h2>Digital Asset Interchange</h2>
                                <div class="text">The future of global innovative digital asset transaction.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                        <div id="image_block_41">
                            <div class="image-box paroller">
                                <figure class="image image-1"><img src="/appway/images/resource/laptop-6x.png" alt=""></figure>
                                <figure class="image image-2"><img src="/appway/images/resource/dashbord-23x.jpeg" alt=""></figure>
                                <figure class="image image-3"><img src="/appway/images/resource/dashbord-24x.jpeg" alt=""></figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- increase-website end -->
        
        <section class="best-hosting">
            <div class="container">
                <div class="sec-title center">
                    <h2>Why Lautan.io?</h2>
                </div>
                <div class="inner-content">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-12 single-column" style="margin-bottom:60px">
                            <div class="single-item wow flipInY" data-wow-delay="00ms" data-wow-duration="1500ms">
                                <div class="inner-box">
                                    <div class="bg-layer" style="background-image: url(/appway/images/icons/pattern-34.png);"></div>
                                    <div class="icon-box"><i class="flaticon-trophy"></i></div>
                                    <h3><a href="#">Easy</a></h3>
                                    <div class="text">All you need is you. From your digital wallet to Lautan.io. Just follow our steps.</div>
                                    <div class="link-btn"><a href="#"><i class="fas fa-long-arrow-alt-right"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                            <div class="single-item wow flipInY" data-wow-delay="300ms" data-wow-duration="1500ms">
                                <div class="inner-box">
                                    <div class="bg-layer" style="background-image: url(/appway/images/icons/pattern-34.png);"></div>
                                    <div class="icon-box"><i class="flaticon-startup"></i></div>
                                    <h3><a href="#">Fast</a></h3>
                                    <div class="text">Quick to withdraw cash into your bank account or top up to IdCash card.</div>
                                    <div class="link-btn"><a href="#"><i class="fas fa-long-arrow-alt-right"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                            <div class="single-item wow flipInY" data-wow-delay="600ms" data-wow-duration="1500ms">
                                <div class="inner-box">
                                    <div class="bg-layer" style="background-image: url(/appway/images/icons/pattern-34.png);"></div>
                                    <div class="icon-box"><i class="flaticon-server-1"></i></div>
                                    <h3><a href="#">Secure</a></h3>
                                    <div class="text">Personal verification send to email and you controlled over your account</div>
                                    <div class="link-btn"><a href="#"><i class="fas fa-long-arrow-alt-right"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                            <div class="single-item wow flipInY" data-wow-delay="300ms" data-wow-duration="1500ms">
                                <div class="inner-box">
                                    <div class="bg-layer" style="background-image: url(/appway/images/icons/pattern-34.png);"></div>
                                    <div class="icon-box"><i class="flaticon-startup"></i></div>
                                    <h3><a href="#">Trusted</a></h3>
                                    <div class="text">Lautan.io is a reliable services, it has been operating with multi layer security.</div>
                                    <div class="link-btn"><a href="#"><i class="fas fa-long-arrow-alt-right"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12 single-column">
                            <div class="single-item wow flipInY" data-wow-delay="600ms" data-wow-duration="1500ms">
                                <div class="inner-box">
                                    <div class="bg-layer" style="background-image: url(/appway/images/icons/pattern-34.png);"></div>
                                    <div class="icon-box"><i class="flaticon-server-1"></i></div>
                                    <h3><a href="#">Convenient</a></h3>
                                    <div class="text">Our professional team offers you  best experienced customer support.</div>
                                    <div class="link-btn"><a href="#"><i class="fas fa-long-arrow-alt-right"></i></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section class="awesome-features">
            <div class="container-fluid">
                <div class="sec-title center">
                    <h2>5 Easy Steps</h2>
                    <p>Trusted by more than 9,000 businesses in 140 countries.<br />all of our resources are free</p>
                </div>
                <div class="inner-content">
                    <div class="four-item-carousel owl-carousel owl-theme">
                        <div class="feature-block-two">
                            <div class="inner-box">
                                <div class="icon-box"><i class="flaticon-graph"></i></div>
                                <h3><a href="#">1. Lautan.io</a></h3>
                                <div class="text">Open account & Verify Account</div>
                                <div class="link-btn"><a href="#"><i class="fas fa-plus"></i></a></div>
                            </div>
                        </div>
                        <div class="feature-block-two">
                            <div class="inner-box">
                                <div class="icon-box"><i class="flaticon-flow-chart"></i></div>
                                <h3><a href="#">2. Your Wallet</a></h3>
                                <div class="text">Top Up from your favorite coin wallet</div>
                                <div class="link-btn"><a href="#"><i class="fas fa-plus"></i></a></div>
                            </div>
                        </div>
                        <div class="feature-block-two">
                            <div class="inner-box">
                                <div class="icon-box"><i class="flaticon-chat"></i></div>
                                <h3><a href="#">3. Lautan.io</a></h3>
                                <div class="text">You can check you balance from our dashbaord</div>
                                <div class="link-btn"><a href="#"><i class="fas fa-plus"></i></a></div>
                            </div>
                        </div>
                        <div class="feature-block-two">
                            <div class="inner-box">
                                <div class="icon-box"><i class="flaticon-businessman"></i></div>
                                <h3><a href="#">4. Withdrawal</a></h3>
                                <div class="text">You can withdraw  to your bank account or idcash.</div>
                                <div class="link-btn"><a href="#"><i class="fas fa-plus"></i></a></div>
                            </div>
                        </div>
                        <div class="feature-block-two">
                            <div class="inner-box">
                                <div class="icon-box"><i class="flaticon-graph"></i></div>
                                <h3><a href="#">5. Enjoy!</a></h3>
                                <div class="text">Enjoy...<br><br></div>
                                <div class="link-btn"><a href="#"><i class="fas fa-plus"></i></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>';
        $web->save();


    }
}
