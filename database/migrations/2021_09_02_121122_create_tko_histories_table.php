<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTkoHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tko_histories', function (Blueprint $table) {
            $table->id();
            $table->integer('active')->default(1)->nullable();
            $table->integer('status')->nullable();
            $table->integer('tko_id')->nullable();
            $table->string('asset')->nullable();
            $table->string('network')->nullable();
            $table->string('address')->nullable();
            $table->string('addressTag')->nullable();
            $table->string('txId')->nullable();
            $table->double('amount')->nullable();
            $table->double('transferType')->nullable();
            $table->string('insertTime')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tko_histories');
    }
}
