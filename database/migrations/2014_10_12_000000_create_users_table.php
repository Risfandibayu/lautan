<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->integer('active')->default(1)->nullable();
            $table->integer('status')->default(1)->nullable();
            $table->string('type')->default('user')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->text('address')->nullable();
            $table->string('nationalId')->nullable();
            $table->string('bankCode')->nullable();
            $table->string('bankName')->nullable();
            $table->string('bankAccount')->nullable();
            $table->string('bankNumber')->nullable();
            $table->string('ipaymuKey')->nullable();
            $table->string('ipaymuVa')->nullable();
            $table->string('tokocryptoKey')->nullable();
            $table->string('fileNationalId')->nullable();
            $table->string('fileBank')->nullable();
            $table->string('filePhoto')->nullable();
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
