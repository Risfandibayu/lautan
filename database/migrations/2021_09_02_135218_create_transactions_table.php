<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->integer('active')->default(1)->nullable();
            $table->integer('status')->default(0)->nullable();
            $table->integer('currency_id')->nullable();
            $table->string('code')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('sender')->nullable();
            $table->string('receiver')->nullable();
            $table->double('amount')->nullable();
            $table->timestamp('success_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
