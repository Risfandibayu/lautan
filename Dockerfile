FROM arakattack/laravel-deployer:7.3
LABEL name="lautan.io" version="2"

WORKDIR /var/www/html
COPY .env.example .env
COPY . /var/www/html
RUN composer self-update --2
RUN COMPOSER_MEMORY_LIMIT=-1 composer install --optimize-autoloader --no-dev
# RUN php artisan route:cache

# RUN php artisan view:clear
# RUN php artisan key:generate
# RUN php artisan optimize
RUN php artisan config:clear
# RUN php artisan queue:restart
# RUN php artisan config:cache
RUN mkdir -p storage/framework/sessions
RUN mkdir -p storage/framework/views
RUN mkdir -p storage/framework/cache
RUN mkdir -p storage/logs
RUN touch storage/logs/laravel.log
RUN chmod -R 777 storage
RUN chmod -R 755 vendor
RUN php artisan storage:link

RUN php artisan vendor:publish --all

RUN composer dump-autoload
