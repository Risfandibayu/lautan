<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Lautan</title>
        <link href="{{asset('public')}}/assets/css/styles.css" rel="stylesheet" />
        <link rel="shortcut icon" href="{{asset('public')}}/favicon.ico" type="image/x-icon">
        <link rel="icon" href="{{asset('public')}}/favicon.ico" type="image/x-icon">
        <script data-search-pseudo-elements="" defer="" src="{{asset('public')}}/assets/js/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js"></script>
    </head>
    <body class="">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container-xl px-4 mt-5">
                        <div class="row justify-content-center">
                            <div class="col-xl-10 col-lg-6 col-md-10 col-sm-12">
                                <!-- Social login form-->
                                <div class="card my-5">
                                    <div class="card-body p-5 text-center">
                                    <img src="{{asset('public/Appway2')}}/images/logo-nav.png" style="height:50px;    width: auto;"></a>
                                    </div>
                                    <hr class="my-0" />
                                    <div class="card-body p-5">
                                       <h1 style="font-weight:bolder;">Privacy Policy</h1>
                                       <br>
                                       <h4>Privacy Policy</h4>
                                       <p>This Privacy Policy governs how Lautan.io collects, uses, maintains and discloses information collected from users (each, "Users") from the website www.Lautan.io ("Site"). This privacy policy applies to the Site and all products and services offered by Lautan.io.</p>

                                       <br>

                                       <h4>Personal Identification Information</h4>

                                       <p>We may collect personally identifiable information from Users in various ways, collecting them into one, but not limiting, when Users visit our site, register on the site, and connect with other similar activities, services, features or resources provide on our Site. The user may be asked to provide a name, and an appropriate email address. Users may visit our site anonymously. We will collect personally identifiable information from Users when they voluntarily send us the information. Users may always refuse to provide personally identifiable information, except that it may prevent it from engaging in activities related to a particular Site. The user has read and agreed to the legal provisions (CP, CPS, Subscriber Agreement, & Privacy Policy) of Peruri CA <a targer="_blank" href="https://ca.peruri.co.id/ca/legal">https://ca.peruri.co.id/ca/legal</a> , as well as the issuance of an Electronic Certificate after successful eKYC.</p>

                                       <br>
                                       <h4>Non-Personal Identification Information</h4>
                                       <p>We may collect non-personally identifiable information about Users whenever they interact with our Site. Non-personally identifiable information may include the browser name, computer type and technical information about the User which means connected to our Site, such as the operating system and the Internet service provider used and other similar information.
                                           <br><br>
                                        In order to provide you with push notification services, our product integrates with email notifications system, which will collect your device information and network information.
                                        <br><br>
                                        <span style="font-weight: bolder">Device information</span>
                                         includes: device identifier (IMEI, IDFA, Android ID, MAC, and OAID), application information (application crash information, notification switch status, and software list), and device parameters & system information (device type, device model, operating system and hardware related information).

                                         <br><br>

                                         <span style="font-weight: bolder">Network information</span> includes: IP address, WiFi information, and base station information.
                                         <br><br>

                                         For your information security, we have cooperated with third-party SDKs. Service providers make data security and confidentiality agreements, and these companies will strictly abide by our data privacy and security requirements. Unless we have your consent, we will not share your personally identifiable information with them. In order for you to better understand the collection of Lautan.io for data types and uses, and how to protect your personal information, you can log in to https://www.lautan.io/privacy-policy.
                                         
                                        </p>
                                        <br>
                                        <h4>Cookie Web Browser</h4>
                                        <p>Our site may use "cookies" to improve the user experience. The user's web browser places a cookie on their hard drive for the purpose of recording and sometimes to track information about it. Users may choose to set their web browser to refuse cookies, or to notify you when a cookie is sent. If they do, note that some parts of the Site may not work properly.</p>
                                        <br>

                                        <h4>How We Use Collected Information</h4>
                                        <p>Lautan.io may collect and use the User's personal information for the following purposes:</p>
                                        <ul>
                                            <li>To process a payment. We may use the information Users provide about themselves when placing an order only to provide such ordering services. We do not share this information with outside parties except to the extent necessary to provide this service.</li>
                                            <li>To run promotions, contests, surveys or other features on this site.</li>
                                            <li>In order to send information to Users, they agree to receive on topics we find interesting to them.</li>
                                            <li>To send periodic emails.</li>
                                            <li>We may use an email address to send User information and updates related to their order. It can also be used to respond to inquiries, queries, and / or other requests. If Users decide to opt in to our mailing list, they will receive emails that may contain company news, updates, product or service related information, etc. If at any time the User wishes to unsubscribe from receiving emails in the future, we include details of the unsubscribe instructions at the bottom of each email.</li>
                                        </ul>
                                        <br>

                                        <h4>How We Protect Your Information</h4>
                                        <p>We apply appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of personal information, usernames, passwords, transaction and data information stored on our Site. <br>
                                            The exchange of sensitive and personal data between the Site and its Users takes place through SSL's secure communications channel and is encrypted and protected by digital signature.
                                            </p>
                                            <br>

                                            <h4>Sharing Your Personal Information</h4>
                                            <p>We do not sell, trade or rent User's personal identification information to anyone. We may share generic aggregated demographic information that is not related to any personal identifying information about visitors and users with our business partners, trusted affiliates and advertisers for the purposes described above.</p>
                                            <br>

                                            <h4>Changes On Privacy Policy</h4>
                                            <p>Lautan.io has the discretion to update this privacy policy at any time. When we do, we will revise the updated date at the bottom of this page. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>

                                            <br>
                                            <h4>Your Acceptance of These Terms</h4>
                                            <p>By using this Site, you signify your acceptance of the policies and terms of this service. If you do not agree to this policy, please do not use our Site. Continuous use of your Site after the announcement of this policy change will be treated as your acceptance of such changes.</p>
                                            <br>

                                            <h4>Contact us</h4>
                                            <p>If you have any questions about this Privacy Policy, the practices of this site, or your transactions with this site, please contact us at:www.Lautan.io</p>
                                            <ul>
                                                <li>
                                                    Lautan Harum Mewangi <br>
Jl. Lebak Sari No.8A, Kerobokan Kelod, Kuta Utara <br>
Kab. Badung, Bali <br>
80361, Indonesia <br>
hello@Lautan.io

                                                </li>
                                            </ul>

                                            <span style="font-style: italic">This document was last updated on June 17, 2021</span>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="footer-admin mt-auto footer-dark">
                    <div class="container-xl px-4">
                        <div class="row">
                            <div class="col-md-6 small">Copyright 漏 Exlaut {{ date("Y") }}</div>
                            <div class="col-md-6 text-md-end small">
                                <a href="#!">Privacy Policy</a>
                                路
                                <a href="#!">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="{{asset('public')}}/assets/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="{{asset('public')}}/assets/js/scripts.js"></script>
</body>

</html>
