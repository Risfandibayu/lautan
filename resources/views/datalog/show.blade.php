@extends('index')
@section('content')
<header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="server"></i></div>
                        Data Log
                    </h1>
                </div>
                <!-- <div class="col-12 col-xl-auto mb-3"><a href="/user/create" class="btn btn-sm btn-light"><i data-feather="plus"></i>&nbsp; Tambah User</a></div> -->
            </div>
        </div>
    </div>
</header>

    <div class="col-md-8 offset-md-2 mt-3 pt-3">
        @if(session('success'))
        <div class="alert alert-success mb-2">
            {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if(session('error'))
        <div class="alert alert-danger mb-2">
            {{ session('error') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        <div class="card mb-3">
            <div class="card-header">
                Detail Log
            </div>

            <div class="table-responsive">
                <table class="table">
                    <tbody>
                        <tr>
                            <td><strong>ID</strong></td>
                            <td>:</td>
                            <td>{{ $row->id }}</td>
                        </tr>
                        <tr>
                            <td><strong>Tanggal</strong></td>
                            <td>:</td>
                            <td>{{ $row->created_at }}</td>
                        </tr>
                        <tr>
                            <td><strong>Channel</strong></td>
                            <td>:</td>
                            <td>{{ $row->channel }}</td>
                        </tr>
                        <tr>
                            <td><strong>Log</strong></td>
                            <td>:</td>
                            <td>{!! nl2br($row->log) !!}</td>
                        </tr>
                        
                    </tbody>
                </table>
            </div>

            <div class="card-footer text-right">
                <a href="/datalog" class="btn btn-dark btn-icon-split">
                    <span class="text">Back</span>
                </a>
            </div>
        </div>
    </div>
@endsection