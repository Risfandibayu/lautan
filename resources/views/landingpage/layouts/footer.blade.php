{!! $footer->content !!}

<div class="modal fade" id="modal-signin" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">

            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12" style="padding:50px">

                            <div class="row">
                                <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                                    <div id="content_block_28">
                                        <div class="content-box" data-wow-delay="00ms" data-wow-duration="1500ms" style="color: white">
                                            <div class="sec-titlex" style="margin-bottom:5px"><h2>Sign In</h2></div>
                                            <div class="text" style="color:#757575;font-size:15px;margin-bottom:15px">Welcome back, please login to your account.</div>

                                            <form method="POST" action="/login">
                                            @csrf
                                                <div class="form-group">
                                                    <label class="text" style="color:#757575;font-size:15px;margin-bottom:0px">Email</label>
                                                    <input type="email" style="background-color:#F5F5F5;color: #333333;font-size:14px;padding:10px 10px;border-radius:8px;border:1px solid #cecece;" class="form-control" name="email">
                                                </div>
                                                <div class="form-group">
                                                    <label class="text" style="color:#757575;font-size:15px;margin-bottom:0px">Password</label>
                                                    <input type="password" style="background-color:#F5F5F5;color: #333333;font-size:14px;padding:10px 10px;border-radius:8px;border:1px solid #cecece;" class="form-control" name="password">
                                                </div>
            
                                                <div><button type="submit" class="btn btn-block" style="background-color:#2F80ED;color:white;padding:10px 40px;border-radius:8px;">Sign In</button></div>
                                            </form>
                                            
                                            <div style="color:#757575;text-align:center;font-size:12px">Don’t have an account? &nbsp;&nbsp; <span style="font-weight:bold;color:#2F80ED" data-toggle="modal" data-target="#modal-signup">Sign Up</span></div>
                                            <hr>
                                            <div><button class="btn btn-block" style="background-color:white;border:1px solid #cecece;font-size:14px;padding:10px 40px;border-radius:8px;">
                                                <img src="https://lautan.s3.amazonaws.com/webcontent/media_QXFyc6.png" style="height:15px;margin-top:-5px;color:#404040;">&nbsp;&nbsp;&nbsp; Sign In with Google</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                                    <img src="https://lautan.s3.amazonaws.com/webcontent/media_pikO8P.png" style="weight: 100%" alt="">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="modal-signup" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">

            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12" style="padding:50px">

                            <div class="row">
                                <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                                    <div id="content_block_28">
                                        <div class="content-box" data-wow-delay="00ms" data-wow-duration="1500ms" style="color: white">
                                            <div class="sec-titlex" style="margin-bottom:5px"><h2>Sign Up</h2></div>
                                            <div class="text" style="color:#757575;font-size:15px;margin-bottom:15px">Please sign up to create your account.</div>

                                            <form method="POST" action="/register">
                                            @csrf
                                                <div class="form-group">
                                                    <label class="text" style="color:#757575;font-size:15px;margin-bottom:0px">Full Name</label>
                                                    <input type="text" style="background-color:#F5F5F5;color: #333333;font-size:14px;padding:10px 10px;border-radius:8px;border:1px solid #cecece;" class="form-control" name="name" placeholder="Enter your name" required>
                                                </div>
                                                <div class="form-group">
                                                    <label class="text" style="color:#757575;font-size:15px;margin-bottom:0px">Phone</label>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input class="form-control" type="text" name="country" value="+62" required readonly style="background-color:#F5F5F5;color: #333333;font-size:14px;padding:10px 10px;border-radius:8px;border:1px solid #cecece;"/>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input class="form-control" type="text" name="phone" placeholder="Please input your phone number" required style="background-color:#F5F5F5;color: #333333;font-size:14px;padding:10px 10px;border-radius:8px;border:1px solid #cecece;margin-left:-20px;"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="text" style="color:#757575;font-size:15px;margin-bottom:0px">Email</label>
                                                    <input type="email" style="background-color:#F5F5F5;color: #333333;font-size:14px;padding:10px 10px;border-radius:8px;border:1px solid #cecece;" class="form-control" name="email" placeholder="Please input your email address" required>
                                                </div>
                                                <div class="form-group">
                                                    <label class="text" style="color:#757575;font-size:15px;margin-bottom:0px">Password</label>
                                                    <input type="password" style="background-color:#F5F5F5;color: #333333;font-size:14px;padding:10px 10px;border-radius:8px;border:1px solid #cecece;" class="form-control" name="password" placeholder="Enter your Password" required>
                                                </div>
                                                <div class="form-group">
                                                    <label class="text" style="color:#757575;font-size:15px;margin-bottom:0px">Confirm Password</label>
                                                    <input type="password" style="background-color:#F5F5F5;color: #333333;font-size:14px;padding:10px 10px;border-radius:8px;border:1px solid #cecece;" class="form-control" name="repassword" placeholder="Confirm Password" required>
                                                </div>
            
                                                <div><button type="submit" class="btn btn-block" style="background-color:#2F80ED;color:white;padding:10px 40px;border-radius:8px;">Sign Up</button></div>
                                            </form>
                                            
                                            <div style="color:#757575;text-align:center;font-size:12px">Already have an account? &nbsp;&nbsp; <span style="font-weight:bold;color:#2F80ED" data-toggle="modal" data-target="#modal-signin">Sign In</span></span></div>
                                            <hr>
                                            <div><button class="btn btn-block" style="background-color:white;border:1px solid #cecece;font-size:14px;padding:10px 40px;border-radius:8px;">
                                                <img src="https://lautan.s3.amazonaws.com/webcontent/media_QXFyc6.png" style="height:15px;margin-top:-5px;color:#404040;">&nbsp;&nbsp;&nbsp; Sign Up with Google</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                                    <img src="https://lautan.s3.amazonaws.com/webcontent/media_xV7Zhu.png" style="weight: 100%" alt="">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!--Scroll to top-->
<button class="scroll-top scroll-to-target" data-target="html">
<span class="fa fa-arrow-up"></span>
</button>


<!-- jequery plugins -->
<script src="/appway/js/jquery.js"></script>
<script src="/appway/js/popper.min.js"></script>
<script src="/appway/js/bootstrap.min.js"></script>
<script src="/appway/js/owl.js"></script>
<script src="/appway/js/wow.js"></script>
<script src="/appway/js/validation.js"></script>
<script src="/appway/js/jquery.fancybox.js"></script>
<script src="/appway/js/appear.js"></script>
<script src="/appway/js/circle-progress.js"></script>
<script src="/appway/js/jquery.countTo.js"></script>
<script src="/appway/js/scrollbar.js"></script>
<script src="/appway/js/jquery.paroller.min.js"></script>
<script src="/appway/js/tilt.jquery.js"></script>
<script src="/appway/js/select2.full.js"></script>
<!-- main-js -->
<script src="/appway/js/script.js"></script>

<script>
    $( ".select2-single").select2({minimumResultsForSearch: Infinity});
    var change = "";
    var changebox = "";
    $(document).on("change", "#idrleft_select", function (){ change = "idrleft_select"; hitung(); });
    $(document).on("change", "#idrright_select", function (){ change = "idrright_select"; hitung(); });
    $(document).on("change", "#curleft_select", function (){ change = "curleft_select"; hitung(); });
    $(document).on("change", "#curright_select", function (){ change = "curright_select"; hitung(); });
    $("#idrleft_price").keyup(function(){ change = "idrleft_price"; hitung(); });
    $("#idrright_price").keyup(function(){ change = "idrright_price"; hitung(); });
    $("#curleft_price").keyup(function(){ change = "curleft_price"; hitung(); });
    $("#curright_price").keyup(function(){ change = "curright_price"; hitung(); });
    
    $(document).on("change", "#changebox", function ()
    {  
        changebox = $('#changebox').val();
        if(changebox == 'BUY') { 
            $('#idrleft').show(); 
            $('#idrright').hide(); 
            $('#curleft').hide(); 
            $('#curright').show(); 
            document.getElementById("btnnya").innerHTML = '<button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal-signin">BUY</button>'; 
            hitung(); 
        }
        else 
        { 
            $('#idrleft').hide(); 
            $('#idrright').show(); 
            $('#curleft').show(); 
            $('#curright').hide(); 
            document.getElementById("btnnya").innerHTML = '<button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#modal-signin">WITHDRAW</button>'; 
            hitung();
        }
    });
    
    function hitung()
    {
        changebox = $('#changebox').val();
        var idrleft_price = $('#idrleft_price').val();
        var idrleft_select = $('#idrleft_select').val();
        var curright_price = $('#curright_price').val();
        var curright_select = $('#curright_select').val();
        var idrright_price = $('#idrright_price').val();
        var idrright_select = $('#idrright_select').val();
        var curleft_price = $('#curleft_price').val();
        var curleft_select = $('#curleft_select').val();
    
        $.ajax(
        {
            type : "GET", url: "/calculate", 
            data : "idrleft_price=" + idrleft_price + "&idrleft_select=" + idrleft_select + "&curright_price=" + curright_price + "&curright_select=" + curright_select + "&idrright_price=" + idrright_price + "&idrright_select=" + idrright_select + "&curleft_price=" + curleft_price + "&curleft_select=" + curleft_select + "&change=" + change + "&changebox=" + changebox,
            success: function(result){
                if(result){ 
                    if(change == 'idrleft_price' || change == 'idrleft_select'){ $('#curright_price').val(result); }
                    else if(change == 'curright_price' || change == 'curright_select'){ $('#idrleft_price').val(result); }
                    else if(change == 'idrright_price' || change == 'idrright_select'){ $('#curleft_price').val(result); }
                    else if(change == 'curleft_price' || change == 'curleft_select'){ $('#idrright_price').val(result); }
                }
            }
        });
    }
</script>

</body>
</html>