@extends('index')
@section('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css" />
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />
@endsection
@section('js')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.9/js/responsive.bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
    $('#table').DataTable({
        responsive: true,
    });
} );
</script>
@endsection
@section('content')
<header class="page-header page-header-compact page-header-light bg-white mt-3">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="clock"></i></div>
                        Transaction History
                    </h1>
                </div>
                <!-- <div class="col-12 col-xl-auto mb-3">Optional page header content</div> -->
            </div>
        </div>
    </div>
</header>
<!-- Main page content-->
<div class="container-xl">
    @if(session('error'))
    <div class="alert alert-danger alert-icon" role="alert">
        <div class="alert-icon-aside">
            <i data-feather="alert-triangle"></i>
        </div>
        <div class="alert-icon-content">
            {{ session('error') }}
        </div>
    </div>
    @endif

    @if(session('success'))
    <div class="alert alert-success alert-icon" role="alert">
        <div class="alert-icon-aside">
            <i data-feather="check-circle"></i>
        </div>
        <div class="alert-icon-content">
            {{ session('success') }}
        </div>
    </div>
    @endif
    <div class="card">
        <!-- <div class="card-header">Example Card</div> -->
        <div class="card-body">
            @if(count($row))
            <table class="table" id='table'>
                <thead>
                    <tr>
                        <th scope="col"></th>
                                                <th scope="col">TRX ID</th>
                        <th scope="col">Coin</th>
                        <th scope="col">Type</th>
                        <th scope="col" class="text-end">Amount</th>
                        <th scope="col" class="text-center">Status</th>
                        <th scope="col">Transaction Date</th>
                        <th scope="col" width="250px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($row as $rows)
                    <tr>
                        <td><img src="{{ $rows->currency->image }}" width="20px"></td>
                        <td>{{ $rows->trx_id }}</td>
                        <td>{{ $rows->code }}</td>
                        <td class="@if($rows->type == 'deposit') text-success @elseif($rows->status == 'withdraw') text-warning @endif">{{ $rows->getType() }}</td>
                        <td class="text-end">{{ $rows->amount }}</td>
                        <td class="@if($rows->status == 1) text-success @elseif($rows->status == 0) text-dark @elseif($rows->status == 2) text-danger @endif text-center">
                            
                            @if($rows->status == 1)
                            Success
                            @elseif($rows->status == 0)
                            Pending
                            @else
                            Reject
                            @endif
                            
                            </td>
                        <td>{{ $rows->created_at }}</td>
                        <td class="text-right">
                            @if($rows->type == 'withdraw' AND $rows->status == 0 AND $rows->cwimg2 != null)
                                <a href="{{ action('TransactionController@wadm_confirm', $rows->id) }}"  class="btn btn-sm btn-outline-primary"><i data-feather="check-circle"></i>&nbsp; Confirmation</a>
                            @elseif($rows->type == 'deposit' AND $rows->status == 0 AND $rows->tximage != null)
                                {{-- <a href="#" data-id="{{ $rows->id }}" data-name="{{ $rows->name }}" class="btn-update btn btn-sm btn-outline-primary"><i data-feather="check-circle"></i>&nbsp; Confirmation</a> --}}
                                <a href="{{ action('TransactionController@adm_confirm', $rows->id) }}"  class="btn btn-sm btn-outline-primary"><i data-feather="check-circle"></i>&nbsp; Confirmation</a>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else 
            <p class="text-center"><small>Transaction not found<small></p>
            @endif
        </div>
    </div>
</div>
<!-- Modal Confirm -->
<div class="modal fade" id="addItem" tabindex="-1" role="dialog" aria-hidden="true">
    <form class="modal-dialog" role="document" method="POST" action="/transaction/confirm" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="id" id="item-id">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="item-title"></h5>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="small mb-1">TX Hash</label>
                        <input type="text" class="form-control form-control-solid" name="txhash" value="" required>
                    </div>
                    <div class="form-group col-md-12 mt-3">
                        <label class="small mb-1">Transaction Screenshot</label>
                        <input class="form-control form-control-solid" name="image" type="file" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" name="save" value="1" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>
@endsection
@push('script')
<script type="text/javascript">
    $('.btn-update').click(function(){
      $('#item-title').html('Transaction Confirmation');
      $('#item-id').val($(this).data("id"));
      $('#addItem').modal('show');        
    });
</script>
@endpush
                