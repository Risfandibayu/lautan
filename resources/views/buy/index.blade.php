@extends('index')
@section('content')
<header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="shopping-bag"></i></div>
                        Beli Kripto
                    </h1>
                </div>
                <!-- <div class="col-12 col-xl-auto mb-3">Optional page header content</div> -->
            </div>
        </div>
    </div>
</header>
<!-- Main page content-->
<div class="container-xl px-4 mt-4">
    <div class="row">
        <div class="offset-xl-3 col-xl-6">
            <div class="card">
                <div class="card-header">Under Development</div>
                <div class="card-body text-center">
                    <img src="{{asset('public')}}/assets/images/coding.png" width="120px">
                    <p class="mt-5">Halaman ini sedang dalam tahap pengembangan</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
                