@include('emails.header')
<p style="box-sizing: border-box; color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 1.5em; margin-top: 0;" align="left">
Dear Customer,<br>
Thank you for submitting Lautan.io Partnership Program application request. We are pleased to invite you to complete the application proposal.
<br><br>
Please find below your link details for your Lautan.io Partner Questionnaire. 
<br><br>
<a href="https://lautan.s3.ap-southeast-1.amazonaws.com/lautan-questionnaire.pdf">https://lautan.io/partner-lautan-questionnaire.pdf</a>
<br><br>
Start to develop your own business, acquired clients and earned commission.
<br><br>
Let’s Connect!
<br><br>
Should you need further assistance, please contact the Client Support Team on email support@lautan.io                             
<br><br>                                       
Kind Regards,<br>
Client Support Team<br><br><br><br>
<hr>
<br><br><br><br>
Pelanggan yang terhormat,<br>
Terima kasih telah mengirimkan permintaan aplikasi Program Partnership Lautan.io. Dengan senang hati kami mengundang Anda untuk melengkapi proposal aplikasi.
<br><br>
Temukan detail tautan Anda untuk Kuesioner Partner Lautan.io dibawah ini.
<br><br>
<a href="https://lautan.s3.ap-southeast-1.amazonaws.com/lautan-questionnaire.pdf">https://lautan.io/partner-lautan-questionnaire.pdf</a>
<br><br>
Mulai kembangkan bisnis Anda sendiri, dapatkan klien, dan dapatkan komisi.
<br><br>
Mari Terhubung!
<br><br>
Jika Anda memerlukan bantuan lebih lanjut, silakan hubungi Client Support Team di email support@lautan.io
<br><br>                                      
Salam Hormat,<br>
Client Support Team<br>

</p>
@include('emails.footer')