@include('emails.header')
<p style="box-sizing: border-box; color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 1.5em; margin-top: 0;" align="left">
    Hi, {{ $name }} <br>
</p>
<p style="box-sizing: border-box; color: #74787E; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size: 14px; line-height: 1.5em; margin-top: 0;" align="left">
    Withdrawal request was rejected. you enter an invalid withdrawal proof. please try to re-upload or request a withdrawal request again.<br><br>
    
    Thank you for using Lautan.io
</p>
@include('emails.footer')