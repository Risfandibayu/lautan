@extends('index')
@section('content')
<header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="user"></i></div>
                        Tambah Partner Role
                    </h1>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Main page content-->
<div class="container-xl px-4 mt-4">
    <div class="row">
        <div class="col-xl-12">
            <form class="card mb-4" method="POST" action="/agentrole">
                @csrf
                <div class="card-header">Tambah Partner Role</div>
                <div class="card-body">
                    @if(session('error'))
                    <div class="alert alert-danger alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="alert-triangle"></i>
                        </div>
                        <div class="alert-icon-content">
                            {{ session('error') }}
                        </div>
                    </div>
                    @endif

                    @if(session('success'))
                    <div class="alert alert-success alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="check-circle"></i>
                        </div>
                        <div class="alert-icon-content">
                            {{ session('success') }}
                        </div>
                    </div>
                    @endif
                    
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label class="small mb-1">Nama</label>
                            <input class="form-control form-control-solid" type="text" placeholder="Masukan nama role" name="name" value="{{ old('name') }}" required />
                        </div>
                        <div class="col-md-6 mb-3">
                            <label class="small mb-1">Fees (%)</label>
                            <input class="form-control form-control-solid" type="text" placeholder="Masukan fees %" name="fees" value="{{ old('fees') }}" required />
                        </div>
                    </div>
                        
                </div>

                <div class="card-footer">
                    <a class="btn btn-light mr-3" href="/agentrole">Batal</a>
                    <button class="btn btn-primary" type="submit">Proses</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
                