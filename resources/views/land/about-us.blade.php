@extends('land.master')
@section('content')
<section class="banner-style-ten"   style="background-image: url({{asset('public/Appway2')}}/images/Ornament11.png);background-repeat: no-repeat;background-position: right 5px top 51px;background-size:10%;padding-top:180px;">
    <div class="container" style="margin-top:-20px;">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                <div id="content_block_28">
                    <div class="content-box wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms">
                        <div class="sec-titlex"><h2>For Everyone to Everyone</h2></div>
                        <div class="text" style="font-size:20px;line-height:20px;">
                            We are building a solution that will bring anyone who believes in crypto to join the digital asset revolution. The world is moving fast on to this revolution at an unprecedented pace.
<br>
<br>
Then here we are, committed to making cryptocurrency accessible for everyone with multiple solutions at your fingertip.
                        </div>
                        <div class="btn-box" style="border-radius: 10px;"><button href="#" data-toggle="modal" data-target="#modal-signin" class="theme-btn-two">Convert Now</button></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                <div id="image_block_27">
                    <div class="image-box js-tilt">
                        <figure class="image clearfix wow slideInRight" data-wow-delay="00ms" data-wow-duration="1500ms">
                            <img src="{{asset('public/Appway2')}}/images/Everyone.png"  style="max-width: 500px;" alt="">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    

<!-- partnership -->
<section class="security-invisible" style="background-image: url({{asset('public/Appway2')}}/images/Ornament11.png);background-repeat: no-repeat;background-position: right 5px top 51px;background-size:10%;margin-top:-150px;margin-bottom: 30px;">
    <div class="container">
        <div class="row align-items-center">

            <div class="col-lg-5 col-md-12 col-sm-12 image-column">
                <div id="image_block_27">
                    <div class="image-box js-tilt">
                        <figure class="image clearfix wow slideInLeft" data-wow-delay="00ms" data-wow-duration="1500ms">
                            <img src="{{asset('public/Appway2')}}/images/rev/01 Core Values.png" style="max-width:500px;" alt="">
                        </figure>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 col-md-12 col-sm-12 content-column">
                <div id="content_block_28">
                    <div class="content-box wow fadeInRight" data-wow-delay="00ms" data-wow-duration="1500ms">
                        <div class="sec-titlex"><h2>The Five Core Values of Lautan.io</h2></div>
                        <div class="text" style="font-size:20px;line-height:20px;">
                            We are committed to earning and maintaining your trust. We believe that in order to do so, we must invest in our 5 core values for the long-term, as they are the inputs that generate a valuable relationship with our clients.
<br><br>
People, Trust, Service, Professionalism and Innovation.
                        </div>
                        <div class="btn-box" style="border-radius: 10px;"><button href="#" data-toggle="modal" data-target="#modal-signin" class="theme-btn-two">Convert Now</button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection