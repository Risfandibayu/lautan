@extends('index')
@section('content')
<header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="user"></i></div>
                        Edit Currency
                    </h1>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Main page content-->
<div class="container-xl px-4 mt-4">
    <div class="row">
        <div class="col-xl-12">
            <form class="card mb-4" method="POST" action="/currency/{{ $row->id }}" enctype="multipart/form-data">
                @csrf
                <input name="_method" type="hidden" value="PUT">
                <div class="card-header">Edit Currency</div>
                <div class="card-body">
                    @if(session('error'))
                    <div class="alert alert-danger alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="alert-triangle"></i>
                        </div>
                        <div class="alert-icon-content">
                            {{ session('error') }}
                        </div>
                    </div>
                    @endif

                    @if(session('success'))
                    <div class="alert alert-success alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="check-circle"></i>
                        </div>
                        <div class="alert-icon-content">
                            {{ session('success') }}
                        </div>
                    </div>
                    @endif
                    
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label class="small mb-1">Coin</label>
                            <input class="form-control form-control-solid" type="text" placeholder="" name="code" value="{{ $row->code }}" required />
                        </div>
                        <div class="col-md-6 mb-3">
                            <label class="small mb-1">Name</label>
                            <input class="form-control form-control-solid" type="text" placeholder="" name="name" value="{{ $row->name }}" required />
                        </div>
                        <div class="col-md-6 mb-3">
                            <label class="small mb-1">Wallet Address</label>
                            <input class="form-control form-control-solid" type="text" placeholder="" name="address" value="{{ $row->address }}" />
                        </div>
                        <div class="col-md-6 mb-3">
                            <label class="small mb-1">Change Icon</label>
                            <input class="form-control form-control-solid" name="icon" type="file">
                        </div>
                        
                    </div>
                        
                </div>

                <div class="card-footer">
                    <a class="btn btn-light mr-3" href="/currency">Batal</a>
                    <button class="btn btn-primary" type="submit">Proses</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
                