<?php $__env->startSection('content'); ?>
<header class="page-header page-header-compact page-header-light bg-white mt-3">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="home"></i></div>
                        Dashboard
                    </h1>
                </div>
                <!-- <div class="col-12 col-xl-auto mb-3">Optional page header content</div> -->
            </div>
        </div>
    </div>
</header>
<!-- Main page content-->
<div class="container-xl">
    <div class="row">
        <div class="col-md-12 text-end">
            <a class="btn btn-sm btn-transparent-dark" href="/market">Show All</a>
        </div>
    </div>
    <div class="row">
        <?php $__currentLoopData = $row; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php 
            $blc = App\Models\Balance::where('user_id',Auth::id())->where('currency_id',$rows->id)->where('active',1)->orderBy('id','DESC')->first();
            if($blc) {
                $binance                 = NEW App\Libs\Binance;
                $send['url']             = '/ticker/24hr';
                $body['symbol']          = $rows->code;
                $send['body']            = $body; 
                $post                    = $binance->callApi($send);

                $balance = $blc->amount;
                $balanceIDR = $post->lastPrice * $blc->amount;
            } else {
                $balance = '0.000000';
                $balanceIDR = 0;
            }
        ?>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <div class="avatar avatar-lg"><img class="avatar-img img-fluid" src="<?php echo e($rows->image); ?>"></div>
                        <div class="ms-3">
                            <div class="fs-4 fw-500"><?php echo e($rows->name); ?></div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <?php if(Auth::user()->type == 'user'): ?>
                            <h5 class="" style="font-size:14px;">Available Balance</h5>
                            <?php else: ?> 
                            <h5 class="" style="font-size:14px;">Portfolio Assets</h5>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-6 text-end">
                            <h5 class="" style="font-size:14px;"><?php echo e($balance); ?> <?php echo e($rows->code); ?></h5>
                            <h4 class="" style="font-size:14px;color:#ccc;">≈ <?php echo e($balanceIDR); ?> IDR</h4>
                        </div>
                    </div>
                    <?php if(Auth::user()->type == 'user'): ?>
                    <hr>
                    <div class="row mt-3">
                        <div class="col-md-6">
                            <a href="/deposit?coin=<?php echo e($rows->code); ?>" class="btn btn-sm btn-outline-primary"><i data-feather="upload"></i>&nbsp; Deposit</a>
                        </div>
                        <div class="col-md-6 text-end">
                            <a href="/withdraw?coin=<?php echo e($rows->code); ?>" class="btn btn-sm btn-outline-primary"><i data-feather="download"></i>&nbsp; Withdraw</a>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>

    <div class="row mt-4">
        <?php if(Auth::user()->type == 'user'): ?>
        <div class="col-md-4">
            <div class="card card-header-actions">
                <div class="card-header">
                   
                    <div>
                        <button class="btn btn-light btn-icon">
                            <i data-feather="shield"></i>
                        </button>
                    </div>
                    Account Verification
                </div>
                <div class="card-body text-center">
                    <?php if(Auth::user()->status == 1): ?>
                        <div class="step step-danger mb-5 mt-4">
                            <div class="step-item active">
                                <a class="step-item-link" href="#" style="font-size:14px;">Unverified</a>
                            </div>
                            <div class="step-item">
                                <a class="step-item-link disabled" href="#" style="font-size:14px;">Verification</a>
                            </div>
                            <div class="step-item">
                                <a class="step-item-link disabled" href="#" style="font-size:14px;">Verified</a>
                            </div>
                        </div>
                        <p>It is recommended to verify to be able to unlock all features</p>
                        <a href="/verification" class="btn btn-sm btn-danger">Verify Now</a>
                    <?php elseif(Auth::user()->status == 2): ?>
                        <div class="step step-warning mb-5 mt-4">
                            <div class="step-item">
                                <a class="step-item-link" href="#" style="font-size:14px;">Unverified</a>
                            </div>
                            <div class="step-item active">
                                <a class="step-item-link" href="#" style="font-size:14px;">Verification</a>
                            </div>
                            <div class="step-item">
                                <a class="step-item-link disabled" href="#" style="font-size:14px;">Verified</a>
                            </div>
                        </div>
                        <p>Your account is under verification, the verification process takes a maximum of 1x24 hours</p>
                    <?php elseif(Auth::user()->status == 3): ?>
                        <div class="step step-success mb-5 mt-4">
                            <div class="step-item">
                                <a class="step-item-link" href="#" style="font-size:14px;">Unverified</a>
                            </div>
                            <div class="step-item">
                                <a class="step-item-link" href="#" style="font-size:14px;">Verification</a>
                            </div>
                            <div class="step-item active">
                                <a class="step-item-link" href="#" style="font-size:14px;">Verified</a>
                            </div>
                        </div>
                        <p>Your account is verified</p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <div class="<?php if(Auth::user()->type == 'user'): ?> col-md-8 <?php else: ?> col-md-12 <?php endif; ?>">
            <div class="card card-header-actions">
                <div class="card-header">
                   
                    <div>
                        <button class="btn btn-light btn-icon">
                            <i data-feather="bar-chart-2"></i>
                        </button>
                    </div>
                    Top Market
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">Coin</th>
                                <th scope="col">Name</th>
                                <th scope="col" class="text-end">Last Price</th>
                                <th scope="col" class="text-end">24H Change</th>
                                <th scope="col" class="text-end">24H High</th>
                                <th scope="col" class="text-end">24H Low</th>
                                <!-- <th scope="col">24H Volume</th> -->
                                <!-- <th scope="col"></th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php $__currentLoopData = $market; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rows): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php 
                                if($rows->change > 0) {
                                    $class = 'text-success';
                                } elseif($rows->change < 0) {
                                    $class = 'text-danger';
                                } else {
                                    $class = '';
                                }
                            ?>
                            <tr>
                                <td><img src="<?php echo e($rows->image); ?>" width="20px"></td>
                                <td><?php echo e($rows->code); ?></td>
                                <td><?php echo e($rows->name); ?></td>
                                <td class="<?php echo e($class); ?> text-end"><?php echo e($rows->price); ?></td>
                                <td class="<?php echo e($class); ?> text-end"><?php echo e($rows->change); ?>%</td>
                                <td class="text-end"><?php echo e($rows->highPrice); ?></td>
                                <td class="text-end"><?php echo e($rows->lowPrice); ?></td>
                                <!-- <td><?php echo e(rupiah($rows->marketPrice)); ?></td> -->
                                <!-- <td><a href="/buy" class="btn btn-sm btn-success"><i data-feather="shopping-bag"></i>&nbsp; Beli</a></td> -->
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
                
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Risfandi Bayu\Downloads\Compressed\exlaut-staging\exlaut-staging\resources\views/dashboard/index.blade.php ENDPATH**/ ?>