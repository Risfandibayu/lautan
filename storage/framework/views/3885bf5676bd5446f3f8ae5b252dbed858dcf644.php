
<?php $__env->startSection('content'); ?>
<section class="banner-style-ten" style="background-color:#F8FBFF;background-image: url(<?php echo e(asset('public/Appway2')); ?>/images/Ornament11.png);background-repeat: no-repeat;background-position: right 5px top 51px;background-size:10%;" >
        <div class="container" style="margin-top:-100px;">
            <div class="row align-items-center">
               
                <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                    <div id="content_block_28">
                        <div class="content-box wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms" style="color: rgb(0, 0, 0)">
                            <div class="sec-titlex">
                                <h2>Partnership<br>
                                <p style="font-size:40px;line-height:35px;">Moving forward together</p></h2>
                            </div>
                            <div class="text" style="font-size:20px;line-height:20px;">Lautan.io is open for partnership and collaboration to take this digital
frontier in new and exciting directions.
<br><br>
Join us on this journey in building the future digital transaction.</div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                    <div id="image_block_27">
                        <div class="image-box js-tilt">
                            <figure class="image clearfix wow slideInRight" data-wow-delay="00ms" data-wow-duration="1500ms"> <img src="<?php echo e(asset('public/Appway2')); ?>/images/rev/01 Partnership .png" style="max-width:500px;" alt=""> </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="security-invisible"  style="color: white;background-image: url(<?php echo e(asset('public/Appway2')); ?>/images/Ornament11.png);background-repeat: no-repeat;background-position: right 5px top 3px;background-size:10%;">
        <div class="container">
            <div class="row align-items-center">
               
                <div class="col-lg-4 col-md-12 col-sm-12 image-column">
                    <div id="image_block_27">
                        <div class="image-box js-tilt" style="text-align:center;">
                            <figure class="image clearfix wow slideInLeft" data-wow-delay="00ms" style="text-align:center;" data-wow-duration="1500ms"> <img src="<?php echo e(asset('public/Appway2')); ?>/images/LAUTs.gif" alt="" style="max-width: 250px;"> </figure>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12 col-sm-12 content-column">
                    <div id="content_block_28">
                        <div class="content-box wow fadeInRight" data-wow-delay="00ms" data-wow-duration="1500ms" style="color: rgb(0, 0, 0)">
                            <div class="sec-titlex">
                                <h3 style="color: rgb(0, 0, 0)">Empowering through collaboration</h3>
                            </div>
                            <div class="text" style="font-size:20px;line-height:20px; color: rgb(0, 0, 0)">Lautan.io is looking for content creators and community leaders who align
with us and having a similar values to promote our services to their
communities.
<br><br>
As we embark on our next phase of growth, we are excited to partner with
like-minded and creative people around the world.
<br><br>
For inquiry and question drop your us an email: marketing@lautan.io</div>
                        </div>
                    </div>
                </div>
                
                
            </div>
            
        </div>
    </section>


    <section class="security-invisible" style="background-image: url(<?php echo e(asset('public/Appway2')); ?>/images/Ornament11.png);background-repeat: no-repeat;background-position: right 5px top 3px;background-size:10%" >
        <div class="container">
            <div class="row align-items-center">
                
                <div class="col-lg-7 col-md-12 col-sm-12 content-column">
                    <div id="content_block_28">
                        <div class="content-box wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms" style="color: rgb(0, 0, 0)">
                            <div class="sec-titlex">
                                <h2>Personal Growth<br>
                                <p style="font-size:40px;line-height:35px;">Start creating your own future</p>
</h2>
                            </div>
                            <div class="text" style="font-size:20px;line-height:20px;">If you want to receive partnership bonuses, you don’t have to create your own affiliate website. Simply copy the link from you’re your account page and share it with your friends or colleagues via an instant messenger, Telegram channel or any other resource that suits you better.<br><br>

                                Please note that spamming, advertising on illegal sites, as well as misleading advertising and any fraud is strictly prohibited.
                                <br><br>
                                Many of our partners have already appreciated all the advantages of Lautan.io’s partnership system and have become our reliable partners!</div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 image-column">
                    <div id="image_block_27">
                        <div class="image-box js-tilt">
                            <figure class="image clearfix wow slideInRight" data-wow-delay="00ms" data-wow-duration="1500ms"> <img src="<?php echo e(asset('public/Appway2')); ?>/images/rev/01 lautan Exchange.png" style="max-width:500px;" alt=""> </figure>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>

    <section class="security-invisible" style="background-color:#F8FBFF;background-image: url(<?php echo e(asset('public/Appway2')); ?>/images/Ornament11.png);background-repeat: no-repeat;background-position: right 5px top 3px;background-size:10%" >
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-12 col-sm-12 image-column">
                    <div id="image_block_27">
                        <div class="image-box js-tilt">
                            <figure class="image clearfix wow slideInLeft" data-wow-delay="00ms" data-wow-duration="1500ms"> <img src="<?php echo e(asset('public/Appway2')); ?>/images/rev/01 3 levels program.png" style="max-width:500px;" alt=""> </figure>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-md-12 col-sm-12 content-column">
                    <div id="content_block_28">
                        <div class="content-box wow fadeInRight" data-wow-delay="00ms" data-wow-duration="1500ms" style="color: rgb(0, 0, 0)">
                            <div class="sec-titlex">
                                <h2>3 Super Level
                                <br>
                                <p style="font-size:26px;line-height:25px;">
Become a partner and start earning passive income</p>
                                </h2>
                            </div>
                            <div class="text" style="font-size:20px;line-height:20px;">Lautan.io Partnership Program has 3 levels – after inviting customer, you will receive not only a percentage of their withdrawal of digital assets but also a percentage from your partnership team.
                                <br><br>
                                The detailed information can be found on your agreement that will be send by email after register as a partner.
                                <br><br>
                                The partnership program is not only profitable but also as transparent as possible – detailed statistics and the number of referrals for each level are available in the personal account of each participant in the “partner dashboard” tab.
                                <br><br>
                                In order to join our partnership program, you must register on the site or enter your personal account.</div>
                            
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('land.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/lautanio/dev.lautan.io/resources/views/land/partnership.blade.php ENDPATH**/ ?>