
<?php $__env->startSection('content'); ?>
<section class="banner-style-ten"   style="background-image: url(<?php echo e(asset('public/Appway2')); ?>/images/Ornament11.png);background-repeat: no-repeat;background-position: right 5px top 51px;background-size:10%;padding-top:180px;">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7 col-md-12 col-sm-12 content-column">
                <div id="content_block_28">
                    <div class="content-box wow fadeInLeft" data-wow-delay="00ms" data-wow-duration="1500ms">
                        <div class="sec-titlex"><h2>Who We Are</h2></div>
                        <div class="text">
                            Lautan.io Indonesia’s forefront of the digital transaction 
                            revolution. Leveraging blockchain technology to make your 
                            transaction more easier, faster, secure, trusted and convenient. 
                            Lautan.io platform is empowering people and businesses to 
                            unite and thrive the global community.
                        </div>
                        <div class="btn-box" style="border-radius: 10px;"><a href="#" data-toggle="modal" data-target="#modal-signin" class="theme-btn-two">Earn With Lautan.io</a></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-12 col-sm-12 image-column">
                <div id="image_block_27">
                    <div class="image-box js-tilt">
                        <figure class="image clearfix wow slideInRight" data-wow-delay="00ms" data-wow-duration="1500ms">
                            <img src="https://lautan.s3.amazonaws.com/webcontent/media_Za2GPL.png" alt="">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    

<!-- partnership -->
<section class="security-invisible" style="background-image: url(<?php echo e(asset('public/Appway2')); ?>/images/Ornament11.png);background-repeat: no-repeat;background-position: right 5px top 51px;background-size:10%;margin-top:-150px;">
    <div class="container">
        <div class="row align-items-center">

            <div class="col-lg-5 col-md-12 col-sm-12 image-column">
                <div id="image_block_27">
                    <div class="image-box js-tilt">
                        <figure class="image clearfix wow slideInLeft" data-wow-delay="00ms" data-wow-duration="1500ms">
                            <img src="https://lautan.s3.amazonaws.com/webcontent/media_1jU7kF.png" alt="">
                        </figure>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 col-md-12 col-sm-12 content-column">
                <div id="content_block_28">
                    <div class="content-box wow fadeInRight" data-wow-delay="00ms" data-wow-duration="1500ms">
                        <div class="sec-titlex"><h2>Our Core Values</h2></div>
                        <div class="text">
                            People, Service, Trust, Innovation, Professionalism.
                        </div>
                        <div class="btn-box" style="border-radius: 10px;"><a href="#" data-toggle="modal" data-target="#modal-signin" class="theme-btn-two">Earn With Lautan.io</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('land.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\dev.lautan.io\resources\views/land/about-us.blade.php ENDPATH**/ ?>