<?php $__env->startSection('content'); ?>
<header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
    <div class="container-xl px-4">
        <div class="page-header-content">
            <div class="row align-items-center justify-content-between pt-3">
                <div class="col-auto mb-3">
                    <h1 class="page-header-title">
                        <div class="page-header-icon"><i data-feather="user"></i></div>
                        Add Currency
                    </h1>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Main page content-->
<div class="container-xl px-4 mt-4">
    <div class="row">
        <div class="col-xl-12">
            <form class="card mb-4" method="POST" action="/currency" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <div class="card-header">Add Currency</div>
                <div class="card-body">
                    <?php if(session('error')): ?>
                    <div class="alert alert-danger alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="alert-triangle"></i>
                        </div>
                        <div class="alert-icon-content">
                            <?php echo e(session('error')); ?>

                        </div>
                    </div>
                    <?php endif; ?>

                    <?php if(session('success')): ?>
                    <div class="alert alert-success alert-icon" role="alert">
                        <div class="alert-icon-aside">
                            <i data-feather="check-circle"></i>
                        </div>
                        <div class="alert-icon-content">
                            <?php echo e(session('success')); ?>

                        </div>
                    </div>
                    <?php endif; ?>
                    
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label class="small mb-1">Coin</label>
                            <input class="form-control form-control-solid" type="text" placeholder="" name="code" value="<?php echo e(old('code')); ?>" required />
                        </div>
                        <div class="col-md-6 mb-3">
                            <label class="small mb-1">Name</label>
                            <input class="form-control form-control-solid" type="text" placeholder="" name="name" value="<?php echo e(old('name')); ?>" required />
                        </div>
                        <div class="col-md-6 mb-3">
                            <label class="small mb-1">Wallet Address</label>
                            <input class="form-control form-control-solid" type="text" placeholder="" name="address" value="<?php echo e(old('address')); ?>" />
                        </div>
                        <div class="col-md-6 mb-3">
                            <label class="small mb-1">Icon</label>
                            <input class="form-control form-control-solid" name="icon" type="file" required>
                        </div>
                        
                    </div>
                        
                </div>

                <div class="card-footer">
                    <a class="btn btn-light mr-3" href="/currency">Cancel</a>
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
                
<?php echo $__env->make('index', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/lautanio/dev.lautan.io/resources/views/currency/create.blade.php ENDPATH**/ ?>